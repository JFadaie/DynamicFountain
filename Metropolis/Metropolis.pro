#-------------------------------------------------
#
# Project created by QtCreator 2015-06-26T18:42:49
#
#-------------------------------------------------
CONFIG   += c++11
QT       += core gui

QMAKE_CXXFLAGS += -std=c++11 -stdlib=libc++ -mmacosx-version-min=10.7
LIBS += -stdlib=libc++ -mmacosx-version-min=10.7

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Metropolis
TEMPLATE = app

INCLUDEPATH += $$PWD/MRGlobal
DEPENDPATH  += $$PWD/MRGlobal
INCLUDEPATH += $$PWD/MRControlPanels
DEPENDPATH  += $$PWD/MRControlPanels
INCLUDEPATH += $$PWD/MRManagement
DEPENDPATH  += $$PWD/MRManagement
INCLUDEPATH += $$PWD/MRGeometryEngine
DEPENDPATH  += $$PWD/MRGeometryEngine
INCLUDEPATH += $$PWD/MRMainWindow
DEPENDPATH  += $$PWD/MRMainWindow


RESOURCES += \
    resources.qrc

DISTFILES += \
    MRGlobal.pri \
    MRControlPanels.pri \
    MRGeometryEngine.pri \
    MRManagement.pri \
    MRMainWindow.pri

include(MRGlobal/MRGlobal.pri)
include(MRControlPanels/MRControlPanels.pri)
include(MRManagement/MRManagement.pri)
include(MRGeometryEngine/MRGeometryEngine.pri)
include(MRMainWindow/MRMainWindow.pri)

FORMS +=

HEADERS +=

SOURCES +=


