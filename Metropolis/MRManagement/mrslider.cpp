#include <QStylePainter>
#include <QStyleOptionSlider>
#include <QRect>
#include <QDebug>

#include "mrslider.h"
#include "qmath.h"

MRSlider::MRSlider( QWidget* pParent ) :
    QSlider( pParent )
{

}

void MRSlider::showEvent( QShowEvent* pEvent )
{
    QSlider::showEvent( pEvent );
  //  qDebug() << "Slider length [show]: " << rect().top() - rect().bottom();
    qDebug() << "Slider left " << rect().left();
    qDebug() << "Slider right " << rect().right();
}

void MRSlider::paintEvent(QPaintEvent* pEvent )
{

    QStylePainter p(this);
    QStyleOptionSlider opt;
    initStyleOption(&opt);

    QRect handle = style()->subControlRect(
                QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);

    // Draw tick marks.
    // Do this manually because they do not display when stylesheets are
    // used.
    int iInterval = tickInterval();
    const int iRange   = maximum() - minimum();

    if (iInterval == 0)
    {
        iInterval = pageStep();
    }

    const int iNumTicks = iRange / iInterval;
    //qDebug() << "tick Interval" << iNumTicks;

    if (tickPosition() != NoTicks)
    {
        int iTickLen = 8;
        int iThickness = 2;

        // Note: The lowest position of the slider is the 'top' and the
        // highest position is the bottom.
        const double dSliderLen    = rect().bottom() - rect().top();

        //qDebug() << "slider length " << dSliderLen;
        const double dDivisionLen  = dSliderLen / iNumTicks;
        for (int i = minimum(); i <= maximum(); i += iInterval)
        {
            int iDivOffset = dDivisionLen * i + rect().top();


            int x = round((double)((double)((double)
                        ( i - minimum() ) / (double)
                        (maximum() - minimum())) * (double)(width() - handle.width()) + (double)(handle.width() / 2.0))) - 1;
            int h = 10;
            p.setPen(QColor("#a5a294"));
            if (tickPosition() == TicksBothSides || tickPosition() == TicksAbove)
            {
                int y = this->rect().top();
                p.drawLine(x, y, x, y + h);
            }
            if (tickPosition() == TicksBothSides || tickPosition() == TicksBelow)
            {
                int y = rect().bottom();
                p.drawLine(x, y, x, y - h);
            }
            if ( tickPosition() == TicksLeft ) {
                int x = this->rect().left();
                //iDivOffset -=
                for ( int t = 0; t < iThickness; ++t )
                    p.drawLine( x, iDivOffset - t, x + iTickLen, iDivOffset + t );
            }
        }
    }

    // draw the slider (this is basically copy/pasted from QSlider::paintEvent)
    opt.subControls = QStyle::SC_SliderGroove;
    p.drawComplexControl(QStyle::CC_Slider, opt);

    // draw the slider handle
    opt.subControls = QStyle::SC_SliderHandle;
    p.drawComplexControl(QStyle::CC_Slider, opt);
}
