

#pragma once

#include <QSlider>

class MRSlider : public QSlider {
    Q_OBJECT
public:
                    MRSlider( QWidget* pParent = 0);
                   ~MRSlider() {}

protected:
    virtual void    paintEvent( QPaintEvent* pEvent );
    virtual void    showEvent(QShowEvent* pEvent );
};

