
#include <QThread>
#include "animationviewer.h"

#define         SLEEP_MSEC          500

AnimationViewer::AnimationViewer(QObject *parent) : QObject(parent)
{
    m_iRepeat = 2;
    m_bStop = false;
}

AnimationViewer::~AnimationViewer()
{

}


void AnimationViewer::setAnimation(
                        int                        iStoredIndex,
                        const QVector<SnapShot *>& rSnapShotList,
                        int                        iRepeat )
{
    m_iStoredIndex = iStoredIndex;
    m_iRepeat = iRepeat;
    m_SnapShotList = rSnapShotList;
}



void AnimationViewer::playAnimation()
{
    // Play the animation for
    while ( !m_bStop ) {
        for (int i = 0; i < m_SnapShotList.size() && !m_bStop; ++i ) {
            SnapShot* const pSnapShot = m_SnapShotList[i];
            emit drawSnapShot( i );
            QThread::currentThread()->msleep( SLEEP_MSEC );
        }
    }

    m_bStop = false;
    emit animationDone( m_iStoredIndex);
}
