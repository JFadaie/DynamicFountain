/**************************************************
 *  File: snapshot.h
 *  Author: Josh Fadaie
 *  Date: 06/27/15
 *  Project: Metropolis
 *
 *  Description: Contains the information necessary to
 *  define a single instance of the fountain.
 *  This class lets the user set the color for the lights,
 *  heights of the pillars, and heights of the water
 *  columns.
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QObject>
#include <QVector>
#include <QVector3D>


class SnapShot : public QObject {
    Q_OBJECT
public:
    explicit                SnapShot(QObject *parent = 0);
                            SnapShot( const SnapShot& rOther );
                            SnapShot( SnapShot&& rOther );

    QVector<QVector3D>      LEDColors() const {
                                   return m_ColorList; }
    QVector<bool>           LEDStates() const {
                                    return m_LEDStates; }
    QVector<QVector3D>      trueLEDColors() const;
    QVector<QVector3D>      lightenAllColors() const;
    QVector<int>            pillarHeights() const {
                                    return m_PillarHList; }
    QVector<int>            waterHeights() const {
                                    return m_WaterHList; }
    void                    setLEDColors(
                                        const QVector<QVector3D>& rColors3D,
                                        const QVector<bool>&      rLEDStates ) {
                                    m_ColorList = rColors3D;
                                    m_LEDStates = rLEDStates; }
    void                    setPillarHeights( const QVector<int>&   rHeights ) {
                                    m_PillarHList = rHeights; }
    void                    setWaterHeights( const QVector<int>&    rHeights ) {
                                    m_WaterHList = rHeights; }

    void                    setFrameElements(
                                            const QVector<QVector3D>& rColors3D,
                                            const QVector<bool>&      rLEDStates,
                                            const QVector<int>&       rHPillars,
                                            const QVector<int>&       rHWaterColumns );
    SnapShot&                   operator= ( SnapShot other );
    friend void                 swap( SnapShot& rSnapShot1, SnapShot& rSnapShot2 );

private:
    // Organized from bottom to top light section.
    QVector<QVector3D>      m_ColorList;

    // Determines whether the color is ON/OFF>
    QVector<bool>           m_LEDStates;

    // Organized by left, center, right pillar column.
    QVector<int>            m_PillarHList;

    // Organized by left, center, right water column.
    QVector<int>            m_WaterHList;

signals:

public slots:                
};

