
#pragma once


#include <QObject>
#include <QVector>
#include "snapshot.h"

class AnimationViewer : public QObject {
    Q_OBJECT

public:
    explicit AnimationViewer( QObject *parent = 0 );
    ~AnimationViewer();
     void setAnimation(
             int                       iCurrentIndex,
             const QVector<SnapShot*>& rSnapShotList,
             int                       iRepeat = 2 );
     void  stopAnimation() { m_bStop = true; }

private:
     int                m_iStoredIndex;
     QVector<SnapShot*> m_SnapShotList;
     int                m_iRepeat;
     bool               m_bStop;

signals:
    void        drawSnapShot( int iSnapShotIndex );
    void        animationDone( int iStoredIndex );

public slots:
    void playAnimation();


};


