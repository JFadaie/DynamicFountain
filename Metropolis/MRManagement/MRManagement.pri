HEADERS += \
    MRManagement/singleton.h \
    MRManagement/snapshot.h \
    $$PWD/mrslider.h \
    $$PWD/animationviewer.h

SOURCES += \
    MRManagement/singleton.cpp \
    MRManagement/snapshot.cpp \
    $$PWD/mrslider.cpp \
    $$PWD/animationviewer.cpp
