/**************************************************
 *  File: snapshot.cpp
 *  Author: Josh Fadaie
 *  Date: 06/27/15
 *  Project: Metropolis
 *
 *  Description: Contains the information necessary to
 *  define a single instance of the fountain.
 *  This class lets the user set the color for the lights,
 *  heights of the pillars, and heights of the water
 *  columns.
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#include <QtAlgorithms>

#include "snapshot.h"
#include "mrglobals.h"

using namespace MR;


/***************************************************************
 * Function: SnapShot::constructor
 *
 * Description: Sets the LED colors, pillar heights and water
 * column heights to their default values.
 */

SnapShot::SnapShot( QObject* pParent ) :
    QObject( pParent )
{

    m_ColorList
            << ColorVector( "DarkGreen")
            << ColorVector( "DarkGreen")
            << ColorVector( "DarkGreen");
    m_LEDStates
            << false << false << false;
    m_PillarHList << 1 << 2 << 3;
    m_WaterHList  << 4 << 3 << 2;
}

SnapShot::SnapShot( const SnapShot& rOther ) : QObject() {
    m_ColorList = rOther.m_ColorList;
    m_LEDStates = rOther.m_LEDStates;
    m_PillarHList = rOther.m_PillarHList;
    m_WaterHList = rOther.m_WaterHList;
}

SnapShot::SnapShot( SnapShot&& rOther ) : QObject()
{
    swap( *this, rOther );
}



/***************************************************************
 * Function: SnapShot::trueLEDColors
 *
 * Description: Returns a copy of the true LED colors. The LEDs
 * states (ON or OFF) are taken into account when determining the
 * resulting color. If the LED is OFF, the color is lighter.
 */

QVector<QVector3D> SnapShot::trueLEDColors() const
{
    QVector<QVector3D> trueColors;
    for ( int i = 0; i < m_LEDStates.size(); ++i ) {
        if ( !m_LEDStates[i] )// If OFF.
            trueColors << MR::LightenColor( m_ColorList[i] );
        else
            trueColors << m_ColorList[i];
    }
    return trueColors;
}

/***************************************************************
 * Function: SnapShot::lightenAllColors
 *
 * Description: Returns a copy of a lighter version of the current
 * LED lights.
 */

QVector<QVector3D> SnapShot::lightenAllColors() const
{
    QVector<QVector3D> lightenedColors;
    for (int i = 0; i < m_ColorList.size(); ++i ) {
        lightenedColors << LightenColor( m_ColorList[i] );
    }
    return lightenedColors;
}


/***************************************************************
 * Function: SnapShot::setFrameElements
 *
 * Description: Sets all of the fountains dynamic properties:
 * LED colors, heights of pillars, and heights of water columns.
 */

void SnapShot::setFrameElements(
                            const QVector<QVector3D>& rColors3D,
                            const QVector<bool>&      rLEDStates,
                            const QVector<int>&       rHPillars,
                            const QVector<int>&       rHWaterCols )
{
    m_ColorList   = rColors3D;
    m_LEDStates   = rLEDStates;
    m_PillarHList = rHPillars;
    m_WaterHList  = rHWaterCols;
}


/***************************************************************
 * Function: SnapShot::assignment operator
 *
 * Description: Assigns snapshot other to this.
 */

SnapShot& SnapShot::operator= ( SnapShot other )
{
    swap( *this, other );
    return *this;
}


/***************************************************************
 * Function: SnapShot::swap
 *
 * Description: Swaps snapshot 1 with snapshot 2.
 */

void swap( SnapShot& rSnapShot1, SnapShot& rSnapShot2 )
{
    qSwap( rSnapShot1.m_ColorList, rSnapShot2.m_ColorList );
    qSwap( rSnapShot1.m_LEDStates, rSnapShot2.m_LEDStates );
    qSwap( rSnapShot1.m_PillarHList, rSnapShot2.m_PillarHList );
    qSwap( rSnapShot1.m_WaterHList, rSnapShot2.m_WaterHList );
}
