#pragma once

#include <QMainWindow>
#include "watercontroller.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    WaterController* m_pWaterCtrl;
public slots:
    void             onEnablePanels( bool bEnable );


};
