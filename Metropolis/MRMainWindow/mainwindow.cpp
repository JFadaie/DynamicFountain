#include <QVBoxLayout>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "glmrwidget.h"
#include "geometryengine.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle( "Metropolis Fountain");

    // Force the layout to be created. Initializes Geometry Engine.
    // NOTE: I might need an init function if this is not a good solution.
    setVisible( false );
    show();
    hide();

    // NOTE: The geometry engine is initialized in the GLCubeWidget during
    // its instantiation.
    GeometryEngine* pEngine = GeometryEngine::getEngine();
    qDebug() << "Size of Geometry Engine: " << pEngine->size();

    // Connect the control manager, frame controller and open gl widget
    // to notify when the current snapshot is updated or changed.
    connect( ui->m_pCtrlManager, &ControlManager::snapShotChanged,
             ui->m_pFrameCtrl,   &FrameController::setSnapShot );
    connect( ui->m_pFrameCtrl, &FrameController::snapShotChanged,
             ui->m_pCtrlManager, &ControlManager::setSnapShot );
    connect( ui->m_pFrameCtrl, &FrameController::snapShotChanged,
             ui->m_pGLMRWidget, &GLMRWidget::setSnapShot );

    // Connect the frame controllers enable/disable panels signal. This signal
    // is emitted when the animation is played.
    //connect( ui->m_pFrameCtrl, &FrameController::enablePanels,
    //         this,             &MainWindow::onEnablePanels );
#if 0
    // Set the Control Widgets
    QVBoxLayout* const pLCtrlDockVLayout = new QVBoxLayout;
    m_pWaterCtrl = new WaterController;
    pLCtrlDockVLayout->addWidget( m_pWaterCtrl );

    ui->m_pLControlDock->setLayout( pLCtrlDockVLayout );
#endif
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::onEnablePanels( bool bEnable )
{
    ui->m_pCtrlManager->setEnabled( bEnable );
    ui->m_pFrameCtrl->setEnabled( bEnable );
}
