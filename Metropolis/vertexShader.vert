#version 330

// Inputs.
in vec3 inPosition3D;
in vec3 inNormal3D;

// This value will be set to 0 if the object's height does not
// need to be offset.
uniform float fYOffset;

// Uniform Matrices
uniform mat4 uMMat;
uniform mat4 uVMat;
uniform mat4 uPMat;

out vec3 ioPos3D_Eye;
out vec3 ioNorm3D_Eye;

void main( void )
{
    // Offset the height (Y-coordinate) of the 3D vertex position.
    vec3 Position3D = inPosition3D;
    Position3D.y = inPosition3D.y + fYOffset;

    ioPos3D_Eye = vec3( uVMat*uMMat*vec4( Position3D, 1.0 ));
    ioNorm3D_Eye = vec3( uVMat*uMMat*vec4( inNormal3D, 0.0 ));

    gl_Position = uPMat*uVMat*uMMat*vec4( Position3D, 1.0 );

}
