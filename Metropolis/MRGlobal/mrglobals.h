/**************************************************
 *  File: qbglobals.h
 *  Author: Josh Fadaie
 *  Date: 06/26/15
 *  Project: Metropolis
 *
 * Description: Header file containing global variables,
 * enums, macros and functions. This file is intended to
 * consolidate commonly used values.
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#define DEF_NUM_SNAPSHOTS               10

// These values are used in the shader program as attribute names.
#define         POS_ATTR_NAME           "inPosition3D"
#define         NORM_ATTR_NAME          "inNormal3D"

// These two macros contain a portion of the name identifying
// objects that have variable heights. These heights for these
// two objects are the same height as the left and right pillars
// middle section and associated middle water section. Names containing
// "middle" are stacked on top of each other to vary heights.
#define         CENTER_WATER_MIDDLE     "Center_Water_Middle"
#define         PILLAR_CENTER_MIDDLE    "PillarCenter_Middle"
#define         VARIABLE_HEIGHT         "Middle"

#define         PILLAR_MAX_HEIGHT       8
#define         PILLAR_MIN_HEIGHT       1
#define         WATER_MAX_HEIGHT        5
#define         WATER_MIN_HEIGHT        0

#include <QVector3D>
#include <QStringList>

namespace MR {
    enum ObjColorType {
        UndefinedClr = 0,
        FountainStand,
        Pillar,
        BottomLights,
        MiddleLights,
        TopLights,
        Water
    };

    enum ObjectId {
        UndefinedID = 0,
        BaseFountainStand,       // 1
        FountainStandWater,      // 2
        StandBottomLights,       // 3
        PillarBaseLights,        // 4
        LeftPillarBase,          // 5
        LeftPillarMiddle,
        LeftPillarTop,
        LeftPillarWaterBase,
        LeftPillarWaterMiddle,
        LeftPillarTopLights,
        CenterPillarBase,
        CenterPillarMiddle,
        CenterPillarTop,
        CenterPillarWaterBase,
        CenterPillarWaterMiddle,
        CenterPillarTopLights,
        RightPillarBase,
        RightPillarMiddle,
        RightPillarTop,
        RightPillarWaterBase,
        RightPillarWaterMiddle, // 21
        RightPillarTopLights    // 22
    };

    enum LightColor {
        GhostWhite  = 0x00D1D1D5,
        LightYellow = 0x00FFFFC0,
        MediumBlue  = 0x000000CD,
        RoyalBlue   = 0x004169E1,
        Navy        = 0x00000080,
        DarkRed     = 0x008B0000,
        Red         = 0x00DD0000,
        OrangeRed   = 0x00FF4500,
        DarkGreen   = 0x00006400,
        Green       = 0x00008000,
        ForestGreen = 0x00228B22,
        LightGrey   = 0x00D3D3D3
    };

    QStringList      GetColors();
    MR::LightColor   ColorId( const QString& rqsColor );
    QVector3D        ColorVector( const QString& rqsColor );
    QVector3D        LightenColor( const QVector3D& rColor3D );
    MR::ObjColorType ObjectColorType( const QString& rqsObjName );
    MR::ObjectId     ObjectID( const QString& rqsObjName );
}
