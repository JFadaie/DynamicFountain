/**************************************************
 *  File: qbglobals.cpp
 *  Author: Josh Fadaie
 *  Date: 06/26/15
 *  Project: Metropolis
 *
 * Description: Header file containing global variables,
 * enums, macros and functions. This file is intended to
 * consolidate commonly used values.
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#include <QString>
#include "mrglobals.h"

// Color masks.
#define   BPOS_MASK         0x0000FF
#define   GPOS_MASK         0x00FF00
#define   GPOS_RSHIFT       8
#define   RPOS_MASK         0xFF0000
#define   RPOS_RSHIFT       16

/***************************************************************
 * Function: MR::ObjectColorType
 *
 * Description: Determines the enumerated object type from the
 * object name.
 */

MR::ObjColorType MR::ObjectColorType( const QString &rqsObjName )
{
    struct ObjId {
        const QString&      rqsId;
        MR::ObjColorType    eType;
    };

    // String contained in objects name is associated with a
    // particular enum. WARNING: The strings below need to be identical
    // to those defined in the blender file/wavefront file. If values
    // are not defined properly, they will be displayed in green.
    const ObjId objIds[] = {
                        { "Pillar",           MR::Pillar },
                        { "Stand",            MR::FountainStand },
                        { "Bottom_Lights",    MR::BottomLights },
                        { "Base_Lights",      MR::MiddleLights },
                        { "TopLights",        MR::TopLights },
                        { "Water",            MR::Water }};

    const int iSize = sizeof( objIds ) / sizeof( objIds[0]);
    for ( int i = 0; i < iSize; ++i ) {
        if ( rqsObjName.contains( objIds[i].rqsId ) )
            return objIds[i].eType;
    }

    return MR::UndefinedClr;
}


/***************************************************************
 * Function: MR::ObjectID
 *
 * Description: Determines the enumerated object type from the
 * object name.
 */

MR::ObjectId MR::ObjectID( const QString &rqsObjName )
{
    struct ObjId {
        const QString& rqsId;
        MR::ObjectId   eType;
    };

    // String contained in objects name is associated with a
    // particular enum. WARNING: The strings below need to be identical
    // to those defined in the blender file/wavefront file. If values
    // are not defined properly, they will be displayed in green.
    const ObjId objIds[] = {
                            { "FPS",    MR::BaseFountainStand },
                            { "FSW",    MR::FountainStandWater },
                            { "BSL",    MR::StandBottomLights },
                            { "BPL",    MR::PillarBaseLights },
                            { "PLB",    MR::LeftPillarBase },
                            { "PLM",    MR::LeftPillarMiddle },
                            { "PLT",    MR::LeftPillarTop },
                            { "LWB",    MR::LeftPillarWaterBase },
                            { "LWM",    MR::LeftPillarWaterMiddle },
                            { "LTL",    MR::LeftPillarTopLights },
                            { "PCB",    MR::CenterPillarBase },
                            { "PCM",    MR::CenterPillarMiddle },
                            { "PCT",    MR::CenterPillarTop },
                            { "CWB",    MR::CenterPillarWaterBase },
                            { "CWM",    MR::CenterPillarWaterMiddle },
                            { "CTL",    MR::CenterPillarTopLights },
                            { "PRB",    MR::RightPillarBase },
                            { "PRM",    MR::RightPillarMiddle },
                            { "PRT",    MR::RightPillarTop },
                            { "RWB",    MR::RightPillarWaterBase },
                            { "RWM",    MR::RightPillarWaterMiddle },
                            { "RTL",    MR::RightPillarTopLights } };

    // Get the first three characters of the name.
    const QString& rqsNameIdentifier = rqsObjName.mid( 0, 3 );
    const int iSize = sizeof( objIds ) / sizeof( objIds[0]);
    for ( int i = 0; i < iSize; ++i ) {
        if ( rqsNameIdentifier == objIds[i].rqsId )
            return objIds[i].eType;
    }

    return MR::UndefinedID;
}


/***************************************************************
 * Function: MR::GetColors
 *
 * Description: Returns a list of all the color names available.
 * for the bottom, middle, and top fountain lights.
 */

QStringList MR::GetColors()
{
    QStringList colorList;
    colorList
            << QString( "GhostWhite" )  << QString( "LightYellow" )
            << QString( "MediumBlue" )  << QString( "RoyalBlue" )
            << QString( "Navy" )        << QString( "DarkRed" )
            << QString( "Red" )         << QString( "OrangeRed" )
            << QString( "DarkGreen" )   << QString( "Green" )
            << QString( "ForestGreen" ) << QString( "LightGrey" );
    return colorList;
}


/***************************************************************
 * Function: MR::ColorId
 *
 * Description: Returns the enumerated color value associated
 * with the provided color name.
 */

MR::LightColor MR::ColorId( const QString& rqsColor )
{

    struct ColorIDs {
        const QString& rqsColor;
        MR::LightColor eColor;
    };

    const ColorIDs colorIds[] = {
                            { "GhostWhite",     MR::GhostWhite },
                            { "LightYellow",    MR::LightYellow },
                            { "MediumBlue",     MR::MediumBlue },
                            { "RoyalBlue",      MR::RoyalBlue },
                            { "Navy",           MR::Navy },
                            { "DarkRed",        MR::DarkRed },
                            { "Red",            MR::Red },
                            { "OrangeRed",      MR::OrangeRed },
                            { "DarkGreen",      MR::DarkGreen },
                            { "Green",          MR::Green },
                            { "ForestGreen",    MR::ForestGreen },
                            { "LightGrey",      MR::LightGrey } };

    // Get the first three characters of the name.
    const int iSize = sizeof( colorIds ) / sizeof( colorIds[0]);
    for ( int i = 0; i < iSize; ++i ) {
        if ( rqsColor == colorIds[i].rqsColor )
            return colorIds[i].eColor;
    }

    return MR::GhostWhite;
}


/***************************************************************
 * Function: MR::ColorVector
 *
 * Description: Returns the 3D color vector associated
 * with the provided color name.
 */

QVector3D MR::ColorVector( const QString &rqsColor )
{
    const float dMaxColor = 255;
    MR::LightColor eColor = MR::ColorId( rqsColor );
    int iColor = (int ) eColor;

    // Get the color value associate with the bit positions for RGB.
    const float dRColor  = ( iColor & RPOS_MASK ) >> RPOS_RSHIFT;
    const float dGColor  = ( iColor & GPOS_MASK ) >> GPOS_RSHIFT;
    const float dBColor  =   iColor & BPOS_MASK;
    return QVector3D(
                dRColor/dMaxColor, dGColor/dMaxColor, dBColor/dMaxColor );

}


/***************************************************************
 * Function: MR::LightenColor
 *
 * Description: Returns a lighter version of the 3D color
 * vector provided.
 */

QVector3D MR::LightenColor( const QVector3D& rColor3D )
{
    const float fCorrectionFactor = 0.40;
    float fRed =
        ( 1.0 - rColor3D.x() ) * fCorrectionFactor + rColor3D.x();
    float fGreen =
        ( 1.0 - rColor3D.y() ) * fCorrectionFactor + rColor3D.y();
    float fBlue =
        ( 1.0 - rColor3D.z() ) * fCorrectionFactor + rColor3D.z();

    fRed = (fRed > 1.0 ) ? 1.0 : fRed;
    fGreen = (fGreen > 1.0 ) ? 1.0 : fGreen;
    fBlue = ( fBlue > 1.0 ) ? 1.0 : fBlue;

    return QVector3D( fRed, fGreen, fBlue );
}
