#version 330

in vec3 ioPos3D_Eye;
in vec3 ioNorm3D_Eye;

out vec4 outColor4D;


// Fixed point light properties
vec3 lightPos3D_Eye = vec3( 0.0, 0.0, 10.0 );

vec3 Ls3D = vec3( 1.0, 1.0, 1.0 );  // White specular color
vec3 Ld3D = vec3( 0.7, 0.7, 0.7 );  // Dull-white diffuse color
vec3 La3D = vec3( 0.2, 0.2, 0.2 );  // Grey ambient color

// Surface Reflectance
vec3 Ks3D = vec3( 1.0, 1.0, 1.0 );  // Fully reflect white specular light
vec3 Kd3D;
vec3 Ka3D = vec3( 1.0, 1.0, 1.0 );  // Fully reflect ambient light
float fSpecularExp = 65.0; // Specular 'power'


// Uniform values
uniform int uiObjClrType;
//uniform int uiLEDState;
uniform vec3 uColor3D;
// Forward Declarations.
void setKd3D(in int iObjClrType );


void main( void )
{
    // Ambient Intensity.
    vec3 Ia3D = La3D * Ka3D;

    // Calculate diffuse intensity
    vec3 distToLight3D_Eye = lightPos3D_Eye - ioPos3D_Eye;
    vec3 dirToLight3D_Eye = normalize( distToLight3D_Eye );

    // Note: Value is between 0 and 1.
    float fIdDotProd = dot( dirToLight3D_Eye, ioNorm3D_Eye );

    // Ensure value is not negative.
    fIdDotProd = max( fIdDotProd, 0.0 );


    // Diffuse Intensity.
    setKd3D( uiObjClrType );
    vec3 Id3D = Ld3D * Kd3D * fIdDotProd;


    // Calculate Specular Intensity.
    vec3 lightReflection3D_Eye = reflect( -dirToLight3D_Eye,
                                          ioNorm3D_Eye );
    vec3 surfaceToViewer3D_Eye = normalize( -ioPos3D_Eye );

    float fIsDotProd = dot( lightReflection3D_Eye,
                            surfaceToViewer3D_Eye );
    fIsDotProd = max( fIsDotProd, 0.0 );
    float fSpecFactor = pow( fIsDotProd, fSpecularExp );

    // Speculat Intensity.
    vec3 Is3D = Ls3D * Ks3D * fSpecFactor;


    //outColor4D = vec4( 0.6, 0.1, 0.9, 1.0 );
    outColor4D = vec4( Is3D + Id3D + Ia3D, 1.0 );

}


// Determines the diffuse color property based on the object type
// chosen for drawing.
void setKd3D(in int iObjClrType ) {

    switch ( iObjClrType ) {
        case 1: {  // Fountain Stand
                // Steel color diffuse reflectance.
                Kd3D = vec3( 0.38, 0.38, 0.50 );
        }
            break;
        case 2: {  // Pillar
                // Steel color diffuse reflectance.
                Kd3D = vec3( 0.38, 0.38, 0.38 );
        }
            break;
        case 3: {  // Bottom Lights.
            // Steel color diffuse reflectance.
            // Kd3D = vec3( 1, 0, 0 );
            Kd3D = uColor3D;
        }
            break;
        case 4: {  // Base Pillar Lights.
            // Steel color diffuse reflectance.
            //Kd3D = vec3( 0, 1, 0 );
            Kd3D = uColor3D;
        }
            break;
        case 5: {  // Top Lights.
            // Steel color diffuse reflectance.
           // Kd3D = vec3( 1, 0, 0 );
            Kd3D = uColor3D;

        }
            break;
        case 6: {  // Water.
            // Blue diffuse surface reflectance.
            Kd3D = vec3( 0.2, 0.6, 1.0 );
        }
            break;
        default: {
            // Green color.
            Kd3D = vec3( 0.0, 0.6, 0.0 );
        }
            break;
    }
}
