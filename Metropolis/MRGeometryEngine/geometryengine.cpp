/***************************************
 *  File: GeometryEngine.cpp
 *  Author: Josh Fadaie
 *  Date: 02/10/2015
 *  Project: QB3D
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#include "geometryengine.h"

#define         NUM_TRIANGLE_VERTS          3


/***************************************************************
 * Function: Constructor
 *
 * Description: Constructor
 *
 */

GeometryEngine::GeometryEngine(QObject* pParent) :
    QObject( pParent )
{
    // These are the default values provided for the vertex, normal,
    // and texel shader attributes. It is expected that these names are
    // used inside the shader program associated with the project that
    // instanciates a GeometryEngine object if no other names are set.
    m_qsPosAttrName = QString( POS_ATTR_NAME );
    m_qsNormalAttrName = QString( NORM_ATTR_NAME );

    m_glBufferUsagePattern = QOpenGLBuffer::StreamDraw;
}


/***************************************************************
 * Function: initEngine
 *
 * Description: Creates the Vertex Array Objects from the data
 * gathered by the Object Processor. This functions gets the
 * data necessary to draw the objects shown on the scene.
 */

void GeometryEngine::initEngine( OBJProcessor &rProcessor,
                                 QOpenGLShaderProgram* pProgram )
{
    for ( int iIndex = 0; iIndex < rProcessor.size(); ++iIndex ) {
        VAOInfo info;
        getProcessorVAOInfo( iIndex, rProcessor, info );
        createVAO( info, pProgram );
    }

    qDebug() << pProgram->log();
}

/***************************************************************
 * Function: Destructor
 *
 * Description: Destructor. Cleans up the data.
 */

GeometryEngine::~GeometryEngine()
{
    // Delete each of the ObjectData structs.
    for ( int iIndex = m_ObjectList.size() - 1; iIndex >= 0; --iIndex ) {
        ObjectData* pObjData = m_ObjectList[iIndex];

        if ( pObjData->pVertexBuffer.isCreated() ) {
            qDebug() << "Vertex Buffer is destroyed!";
            pObjData->pVertexBuffer.destroy();
        }

        if ( pObjData->pNormalBuffer.isCreated() ) {
            pObjData->pNormalBuffer.destroy();
        }

        pObjData->pVAO->destroy();

        delete pObjData;
    }
}


/***************************************************************
 * Function: GeometryEngine::getProcessorVAOInfo
 *
 * Description: Organizes the information from the Object Processor
 * into a packet of information used to construct a Vertext Array
 * Object.
 */

void GeometryEngine::getProcessorVAOInfo(
                                          int           iObjIndex,
                                          OBJProcessor& rProcessor,
                                          VAOInfo&      info )
{
    info.pfVertexList = rProcessor.getVerticesData( iObjIndex );
    //printVec3DList( info.pfVertexList, "Info Vertex List" );
    info.pfNormalList = rProcessor.getNormalsData( iObjIndex );
    info.qsObjName = rProcessor.getObjName( iObjIndex );

    info.eID = MR::ObjectID( info.qsObjName );

    if ( info.qsObjName.contains( VARIABLE_HEIGHT ))
        info.fHeightY = rProcessor.calcObjectHeight( iObjIndex );
    else
        info.fHeightY = -1;
}



/***************************************************************
 * Function: GeometryEngine::createVAO
 *
 * Description: Creates the vertex array object with the provided data.
 * The VAO contains the normals and vertex positions of the
 * displayed object. The VAO is then stored inside a struct associated
 * with the object. The struct contains other object defining data
 * such as its height (if necessary), and name.
 */

void GeometryEngine::createVAO( VAOInfo& rInfo,
                                QOpenGLShaderProgram* pProgram )
{

    Q_ASSERT( pProgram != 0 );

    if ( rInfo.pfVertexList == 0 ) {
        qDebug() << "Warning!: No vertex data... returning.";
        return;
    }

    // Stores VAO, VBO(s), Object Name, center, etc. for each object.
    ObjectData* pObjData = new ObjectData;

    // Create VAO which stores VBO information for convenient access
    // and rendering of data.
    QOpenGLVertexArrayObject* pVAO = new QOpenGLVertexArrayObject( this );
    pVAO->create();
    pVAO->bind();

    // Get the position of the Buffer Object's associated shader program
    // attribute.
    m_gluiPosAttribute = pProgram->attributeLocation( m_qsPosAttrName );

    // Create, bind and allocate data for the vertex buffer.
    QOpenGLBuffer vertexBuffer = QOpenGLBuffer( QOpenGLBuffer::VertexBuffer );
    vertexBuffer.create();
    vertexBuffer.setUsagePattern( m_glBufferUsagePattern );
    vertexBuffer.bind();
   // printVec3DList( rInfo.pfVertexList, "Vertex List - In VAO Creation" );
    vertexBuffer.allocate( (*rInfo.pfVertexList).data(),
                           rInfo.pfVertexList->size()*sizeof(QVector3D ));

    // Associate the position attribute with the currently bound buffer object.
    // Enable the attribute array pointer to this buffer object in the VAO.
    pProgram->setAttributeBuffer( m_gluiPosAttribute, GL_FLOAT, 0, 3, sizeof (QVector3D));
    pProgram->enableAttributeArray( m_gluiPosAttribute );

    // Get the size of the objects data.
    pObjData->iNumTriangles = rInfo.pfVertexList->size();
    qDebug() << "Vertex List Size " << rInfo.pfVertexList->size();
    qDebug() << "Vertex Buffer Size" << vertexBuffer.size();
    // Store reference to object's vertex buffer.
    pObjData->pVertexBuffer = vertexBuffer;

    if ( rInfo.pfNormalList != 0 ) {

        m_gluiNormalAttribute = pProgram->attributeLocation( m_qsNormalAttrName );

        QOpenGLBuffer normalBuffer = QOpenGLBuffer( QOpenGLBuffer::VertexBuffer );
        normalBuffer.create();
        normalBuffer.setUsagePattern( m_glBufferUsagePattern );
        normalBuffer.bind();
        normalBuffer.allocate( (*rInfo.pfNormalList).data(),
                               rInfo.pfNormalList->size()*sizeof(QVector3D ));

        pProgram->setAttributeBuffer( m_gluiNormalAttribute, GL_FLOAT, 0, 3, sizeof(QVector3D));
        pProgram->enableAttributeArray( m_gluiNormalAttribute );

        pObjData->pNormalBuffer = normalBuffer;

    }


    // Store other object info: name, and height.
    pObjData->qsObjName = rInfo.qsObjName;
    pObjData->fHeightY  = rInfo.fHeightY;
    pObjData->eID       = rInfo.eID;

    // Set VAO pointer.
    pObjData->pVAO = pVAO;
    // Add object data pointer to list containing each object's data.
    m_ObjectList << pObjData;

    // Unbind this VAO from the context.
    pVAO->release();

}


/***************************************************************
 * Function: GeometryEngine::drawObject
 *           GeometryEngine::drawLeftPillar
 *           GeometryEngine::drawCenterPillar
 *           GeometryEngine::drawRightPillar
 *           GeometryEngine::drawMiddleLighting
 *           GeometryEngine::drawFountainStand
 *
 * Description: Convenience functions to draw the objects onto the
 * screen.
 *
 * Parameters:
 *      rProgram: Opengl shader program that allows the setting of values
 *                used in the shader programs (vertex and fragement ).
 *      iObjectId: integer representation of the enum ObjectId. This id
 *                 corresponds to the object drawn.
 *      iHYPillar: Number of times the middle unit is drawn. If the number is
 *                 0, the height of the middle unit is subtracted from the
 *                 positions of the preceding objects.
 */

void GeometryEngine::drawObject( int iObjIndex )
{
    ObjectData* pObjData = m_ObjectList[iObjIndex];
    pObjData->pVAO->bind();

    glDrawArrays( GL_TRIANGLES, 0, pObjData->iNumTriangles );

}

void GeometryEngine::drawObject(
                                QOpenGLShaderProgram& rProgram,
                                MR::ObjectId          eObjectId,
                                float                 fOffsetY )
{
    ObjectData* pObjData = objectData( eObjectId );

    int iObjColorType = (int ) MR::ObjectColorType( pObjData->qsObjName );
    rProgram.setUniformValue( "uiObjClrType", iObjColorType );
    rProgram.setUniformValue( "fYOffset", fOffsetY );
    pObjData->pVAO->bind();

    glDrawArrays( GL_TRIANGLES, 0, pObjData->iNumTriangles );
}

void  GeometryEngine::drawLeftPillar(
                QOpenGLShaderProgram&   rProgram,
                const int               iHYPillar,
                const int               iHYWater,
                const QVector3D&        rTopClrVec )
{
    // Get the height of the middle pillar.
    ObjectData* const pPillarMiddle = objectData( MR::LeftPillarMiddle );
    const float fPillarMiddleHeight = pPillarMiddle->fHeightY;

    // Get the height of a single unit of water.
    ObjectData* const pWaterMiddle = objectData( MR::LeftPillarWaterMiddle );
    const float fWaterMiddleHeight = pWaterMiddle->fHeightY;

    drawObject( rProgram, MR::LeftPillarBase );

    // Draw the middle pillar. Consequtive drawings of the middle pillar
    // object are offset by the combined height of the previous middle
    // units drawn.
    float fTotalPillarOffset = -1;
    for ( int i = 0; i < iHYPillar; ++i ) {
        fTotalPillarOffset = i * fPillarMiddleHeight;
        drawObject( rProgram, MR::LeftPillarMiddle, fTotalPillarOffset );
    }

    // If the total offset is -1, indicating that no middle unit was
    // drawn, the height of the preceding objects should be offset
    // negatively to compensate.
    if ( fTotalPillarOffset == -1 )
        fTotalPillarOffset = -1 * fPillarMiddleHeight;

    drawObject( rProgram, MR::LeftPillarTop, fTotalPillarOffset );

    drawObject( rProgram, MR::LeftPillarWaterBase, fTotalPillarOffset );

    rProgram.setUniformValue( "uColor3D", rTopClrVec );
    drawObject( rProgram, MR::LeftPillarTopLights, fTotalPillarOffset );

    // Draw the middle water object. Consequtive drawings of the middle water
    // object are offset by the combined height of the previous middle
    // water units drawn and the height of the middle pillar units.
    for ( int i = 0; i < iHYWater; ++i ) {
        float fTotalWaterOffset = i * fWaterMiddleHeight;
        fTotalWaterOffset += fTotalPillarOffset;
        drawObject( rProgram, MR::LeftPillarWaterMiddle, fTotalWaterOffset );
    }
}

void  GeometryEngine::drawCenterPillar(
                QOpenGLShaderProgram&   rProgram,
                const int               iHYPillar,
                const int               iHYWater,
                const QVector3D&        rTopClrVec )
{
    // Get the height of the middle pillar.
    ObjectData* const pPillarMiddle = objectData( MR::CenterPillarMiddle );
    const float fPillarMiddleHeight = pPillarMiddle->fHeightY;

    // Get the height of a single unit of water.
    ObjectData* const pWaterMiddle = objectData( MR::CenterPillarWaterMiddle );
    const float fWaterMiddleHeight = pWaterMiddle->fHeightY;

    drawObject( rProgram, MR::CenterPillarBase );

    // Draw the middle pillar. Consequtive drawings of the middle pillar
    // object are offset by the combined height of the previous middle
    // units drawn.
    float fTotalPillarOffset = -1;
    for ( int i = 0; i < iHYPillar; ++i ) {
        fTotalPillarOffset = i * fPillarMiddleHeight;
        drawObject( rProgram, MR::CenterPillarMiddle, fTotalPillarOffset );
    }

    // If the total offset is -1, indicating that no middle unit was
    // drawn, the height of the preceding objects should be offset
    // negatively to compensate.
    if ( fTotalPillarOffset == -1 )
        fTotalPillarOffset = -1 * fPillarMiddleHeight;

    drawObject( rProgram, MR::CenterPillarTop, fTotalPillarOffset );

    drawObject( rProgram, MR::CenterPillarWaterBase, fTotalPillarOffset );

    rProgram.setUniformValue( "uColor3D", rTopClrVec );
    drawObject( rProgram, MR::CenterPillarTopLights, fTotalPillarOffset );

    // Draw the middle water object. Consequtive drawings of the middle water
    // object are offset by the combined height of the previous middle
    // water units drawn and the height of the middle pillar units.
    for ( int i = 0; i < iHYWater; ++i ) {
        float fTotalWaterOffset = i * fWaterMiddleHeight;
        fTotalWaterOffset += fTotalPillarOffset;
        drawObject( rProgram, MR::CenterPillarWaterMiddle, fTotalWaterOffset );
    }
}

void  GeometryEngine::drawRightPillar(
                QOpenGLShaderProgram&   rProgram,
                const int               iHYPillar,
                const int               iHYWater,
                const QVector3D&        rTopClrVec )
{
    // Get the height of the middle pillar.
    ObjectData* const pPillarMiddle = objectData( MR::RightPillarMiddle );
    const float fPillarMiddleHeight = pPillarMiddle->fHeightY;

    // Get the height of a single unit of water.
    ObjectData* const pWaterMiddle = objectData( MR::RightPillarWaterMiddle );
    const float fWaterMiddleHeight = pWaterMiddle->fHeightY;

    drawObject( rProgram, MR::RightPillarBase );

    // Draw the middle pillar. Consequtive drawings of the middle pillar
    // object are offset by the combined height of the previous middle
    // units drawn.
    float fTotalPillarOffset = -1;
    for ( int i = 0; i < iHYPillar; ++i ) {
        fTotalPillarOffset = i * fPillarMiddleHeight;
        drawObject( rProgram, MR::RightPillarMiddle, fTotalPillarOffset );
    }

    // If the total offset is zero, indicating that no middle unit was
    // drawn, the height of the preceding objects should be offset
    // negatively to compensate.
    if ( fTotalPillarOffset == -1 )
        fTotalPillarOffset = -1 * fPillarMiddleHeight;

    drawObject( rProgram, MR::RightPillarTop, fTotalPillarOffset );

    drawObject( rProgram, MR::RightPillarWaterBase, fTotalPillarOffset );

    rProgram.setUniformValue( "uColor3D", rTopClrVec );
    drawObject( rProgram, MR::RightPillarTopLights, fTotalPillarOffset );

    // Draw the middle water object. Consequtive drawings of the middle water
    // object are offset by the combined height of the previous middle
    // water units drawn and the height of the middle pillar units.
    for ( int i = 0; i < iHYWater; ++i ) {
        float fTotalWaterOffset = i * fWaterMiddleHeight;
        fTotalWaterOffset += fTotalPillarOffset;
        drawObject( rProgram, MR::RightPillarWaterMiddle, fTotalWaterOffset );
    }
}

void  GeometryEngine::drawMiddleLighting(
                QOpenGLShaderProgram&   rProgram,
                const QVector3D&        rMiddleClrVec)
{
    rProgram.setUniformValue( "uColor3D", rMiddleClrVec );
    drawObject( rProgram, MR::PillarBaseLights );
}


void  GeometryEngine::drawFountainStand(
                 QOpenGLShaderProgram& rProgram,
                 const QVector3D&      rBaseClrVec )
{
    drawObject( rProgram, MR::BaseFountainStand );

    drawObject( rProgram, MR::FountainStandWater );

    rProgram.setUniformValue( "uColor3D", rBaseClrVec );
    drawObject( rProgram, MR::StandBottomLights );
}


/***************************************************************
 * Function: GeometryEngine::getObjectData
 *
 * Description: Retrieves data the data struct associated with
 * an object at the provided index.
 */

ObjectData& GeometryEngine::getObjectData( int iObjIndex )
{
    Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_ObjectList.size() );
    return (*m_ObjectList[iObjIndex]);
}

/***************************************************************
 * Function: GeometryEngine::objectData
 *
 * Description: Retrieves data the data struct associated with
 * provided object id.
 */

ObjectData* GeometryEngine::objectData( MR::ObjectId eObjectId )
{
    ObjectData* pObjData = nullptr;
    for ( int i = 0; i < m_ObjectList.size(); ++i ) {
        if ( m_ObjectList[i]->eID == eObjectId ) {
            pObjData = m_ObjectList[i];
            break;
         }
    }
    return pObjData;
}

/***************************************************************
 * Function: GeometryEngine::getObjectName
 *
 * Description: Gets the name of the object at the specifed index.
 */

QString GeometryEngine::getObjectName( int iObjIndex )
{
    Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_ObjectList.size() );
    ObjectData* pObjData = m_ObjectList[iObjIndex];
    return pObjData->qsObjName;
}


void GeometryEngine::printVec3DList( QVector<QVector3D> *pVec3DList,
                                     const QString& rqsDebugTitle )
{
    qDebug() << "\nBegin Debugging: " << rqsDebugTitle;
    for ( int iIndex = 0; iIndex < pVec3DList->size(); ++iIndex ) {
        qDebug() << pVec3DList->at( iIndex );
    }
    qDebug() << "End Debugging: " << rqsDebugTitle << "\n";
}

