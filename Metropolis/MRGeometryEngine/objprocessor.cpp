/***************************************
 *  File: objprocesser.cpp
 *  Author: Josh Fadaie
 *  Date: 06/26/2015
 *  Project: Metropolis
 *
 *  Description: Parses through a given wavefront file
 *  for the texel, normal and vertex data.
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#include <assert.h>

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <qmath.h>
#include <QVector2D>
#include <QtGlobal>
#include <QByteArray>

#include "objprocessor.h"

#define     NUM_VERT_DIM    3
#define     NUM_NORM_DIM    3
#define     NUM_TEXEL_DIM   2
#define     MAX_DIM         3


/***************************************************************
 * Function: Constructor
 *
 * Description: Constructor
 *
 */

OBJProcessor::OBJProcessor(
                        const QString& rqsFileName,
                        QObject*       pParent ) :
    QObject( pParent )
{// Temporary storage containers.
    QVector<QVector3D> fPosVector;
    QVector<QVector3D> fNormalsVector;
    QVector<QVector2D> fTexelsVector;
    QVector<QVector3D> iFacesVector;

    QFile objFile( rqsFileName );

    if ( !objFile.open( QIODevice::ReadWrite | QIODevice::Text )) {
         qDebug() << "The File did not open successfully... returning.";
         return;
    }

    QTextStream inStream( &objFile );
    int iNewObject = -1;  // Looks for 'o' in file. Used to store data in different
                         // buffer for each seperate object. Starts at -1 so internal
                         // loop does not restart initially once 'o' is found. Once count is
                        // to 1, indicating a new object, the count is reset to zero. Acts as a bool,
                        // except for the first object.

    // Read each line of text until the end of the file.
    while ( !inStream.atEnd() ) {

        // Read each line of text until a new object or the end of the file.
        while( iNewObject < 1 && !inStream.atEnd() ) {
            QString qsLine = inStream.readLine();
           // qDebug() << qsLine;
            if ( !qsLine.isEmpty() && qsLine[0] == 'o' ) {
                iNewObject++;

                // Get object's name here.
                QString qsObjName = qsLine.mid(2).trimmed();
                m_FountainObjNames << qsObjName;
                continue;
            }
            if ( !qsLine.isNull() ) {
                getOBJLineData( qsLine,
                                fPosVector,
                                fNormalsVector,
                                fTexelsVector,
                                iFacesVector );
            }
        }

        processVNOBJData( fPosVector, fNormalsVector, iFacesVector );

        // Clear face vector as data at indices have already been stored.
        iFacesVector.clear();

        // Reset flags and increment buffer index.
        iNewObject = 0;
     }

     objFile.close();
}


/***************************************************************
 * Function: Destructor
 *
 * Description: Deletes the pointers containing the vertex,
 * normal, and texture coordinates.
 *
 */

OBJProcessor::~OBJProcessor()
{
    // Start iterating from the back.
    for (int iIndex = m_FountainObjBuf.size() - 1;
         iIndex >= 0; --iIndex ) {

        ObjDataPtrs objPtrs = m_FountainObjBuf[iIndex];
        delete objPtrs.pVertexData;
        delete objPtrs.pNormalData;
    }
}


/***************************************************************
 * Function: OBJProcessor::getOBJLineData
 *
 * Description: Processes the line of data provided token by token,
 *              which are seperated by whitespace. If the data contained
 *              in each specifier is valid it is stored in the appropriate
 *              data buffer.
 *
 * Parameters:
 *      rqsLine: The QString reference that is processed. The line
 *               cannot be null, but
 */

void OBJProcessor::getOBJLineData( QString& rqsLine,
                                   QVector<QVector3D>& rfPosVector,
                                   QVector<QVector3D>& rfNormalsVector,
                                   QVector<QVector2D>& rfTexelsVector,
                                   QVector<QVector3D>& riFacesVector ) {

    assert( !rqsLine.isNull() );

    QString qsType = rqsLine.mid( 0 , 2 );
    QString qsData = rqsLine.mid( 2 );
    qsData = qsData.trimmed();

    // Vertex, Texel, or Normal data.
    if(  qsType.compare("v ") == 0 ||
         qsType.compare("vt") == 0 ||
         qsType.compare("vn") == 0 ) {
        QStringList qsList = qsData.split( " " );
        QVector3D dataVec3D;
        dataVec3D.setX( qsList[0].toFloat() );
        dataVec3D.setY( qsList[1].toFloat() );
        if ( qsList.size() >= MAX_DIM ) {
            dataVec3D.setZ( qsList[2].toFloat() );
        }


        if( qsType.compare("v ") == 0 ) {
             rfPosVector << dataVec3D;
            // qDebug() << "V: " << dataVec3D;
        } else if( qsType.compare("vt") == 0 ) {
             QVector2D dataVec2D = dataVec3D.toVector2D();
            // qDebug() << "Vt: " << dataVec2D;
             rfTexelsVector << dataVec2D;
        } else if( qsType.compare("vn") == 0 ) {
            // qDebug() << "Vn: " << dataVec3D;
             rfNormalsVector << dataVec3D;
        }

    } else if( qsType.compare("f ") == 0 ) {

        // Split data into list of v/vt/vn first
        QStringList qsList = qsData.split( " " );
        QStringListIterator iGroup( qsList );

        // Split the line of face data into 3 seperate int values.
        while( iGroup.hasNext()) {
            QString qsFaceData = iGroup.next();
            //qDebug() << qsFaceData;
            QStringList qsList = qsFaceData.split( "/" );
            QVector3D faceDataVec3D;
            faceDataVec3D.setX( qsList[0].toFloat());
            faceDataVec3D.setY( qsList[1].toFloat());
            faceDataVec3D.setZ( qsList[2].toFloat());

           // qDebug() << "F: " << faceDataVec3D;
            // store 3D vector containing v/vt/n data.
            riFacesVector << faceDataVec3D;
        }
    }
}


/***************************************************************
 * Function: OBJProcessor::processVNOBJData
 *
 * Description: Takes the indices provided in the faces vector
 * to retrieve the corresponding vertex or normal value
 * Parameters:
 *
 */

void OBJProcessor::processVNOBJData( QVector<QVector3D> &rfPosVector,
                                     QVector<QVector3D> &rfNormalsVector,
                                     QVector<QVector3D> &riFacesVector)
{
    // Containers for the new object's vertex, texel, and normal data.
    //const int iNSize = rfNormalsVector.size();
    //const int iPSize = rfPosVector.size();
    QVector<QVector3D>* pObjPosData =    new QVector<QVector3D>(  );
    QVector<QVector3D>* pObjNormalData = new QVector<QVector3D>( );

    // Set object data pointers.
    ObjDataPtrs objData;
    objData.pVertexData = pObjPosData;
    objData.pNormalData = pObjNormalData;

    // Store new pointers at appropriate index of object data buffer.
    m_FountainObjBuf << objData;

    qDebug() << "\n\n" << "BEGIN PROCESSING";
    for ( int iIndex = 0; iIndex < riFacesVector.size(); ++iIndex ) {

        // Get the v and vn values.
        // Decrement value by 1, and multiply by offset. Remember each,
        // indice represents a group of vertex, normal, and texel dimensions.
        // Vertex: XYZ, Normal: IJK, Texels: UV.
        QVector3D faceData3D = riFacesVector[iIndex];

        int iVIndex0 = faceData3D.x() - 1;   // 'v'
        int iNIndex0 = faceData3D.z() - 1;   // 'vn'

        // Get 3D Vector values at index. Create copy and
        // store data in object's data pointer.
        QVector3D posData3D = rfPosVector[iVIndex0];
      //  qDebug() << "V Index: " << iVIndex0;
      //  qDebug() << "V Value: " << posData3D;
        (*pObjPosData ) << posData3D;

        QVector3D normalData3D = rfNormalsVector[iNIndex0];
      //  qDebug() << "N Index: " << iNIndex0;
      //  qDebug() << "N Value: " << normalData3D;
        (*pObjNormalData ) << normalData3D;
    }

    qDebug() << "END PROCESSING" << "\n\n";
}


/***************************************************************
 * Function: OBJProcessor::getVerticesData
 *           OBJProcessor::getNormalsData
 *
 * Description: Retrieve the data pointers corresponding to the
 *              object at the provided index. Assertes that the
 *              index provided is within the bounds of the vector.
 *
 * Parameters:
 *      iObjIndex: index of the object.
 */

QVector<QVector3D>* OBJProcessor::getVerticesData( int iObjIndex )
{
   Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_FountainObjBuf.size() );

   ObjDataPtrs objData = m_FountainObjBuf[iObjIndex];
   return objData.pVertexData;
}

QVector<QVector3D>* OBJProcessor::getNormalsData( int iObjIndex )
{
   Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_FountainObjBuf.size() );

   ObjDataPtrs objData = m_FountainObjBuf[iObjIndex];
   return objData.pNormalData;
}


/***************************************************************
 * Function: OBJProcessor::calcObjectHeights
 *
 * Description: Returns the height (Y Direction ) of the object with
 * the provided index.
 *
 * Parameters:
 *      iObjIndex: index of the object.
 */

float OBJProcessor::calcObjectHeight( int iObjIndex )
{
   Q_ASSERT( iObjIndex >= 0 && iObjIndex < m_FountainObjBuf.size() );

    QVector<QVector3D>* const pPosData3D = getVerticesData( iObjIndex );

    if ( pPosData3D == 0 || pPosData3D->isEmpty() ) {
        qDebug() << "Vector is emtpy at index: " << iObjIndex;
        return -1;
    }

    // Get first vertex in list. Prime the min and max.
    const QVector3D vertex3D0 = pPosData3D->at( 0 );
    float fMinY = vertex3D0.y();
    float fMaxY = fMinY;

    // Calculate the new min and max Y coordinate position.
    for ( int iIndex = 1; iIndex < pPosData3D->size(); ++iIndex ) {
        QVector3D vertex3D = pPosData3D->at( iIndex );
        fMinY = qMin( vertex3D.y(), fMinY );
        fMaxY = qMax( vertex3D.y(), fMaxY );
    }

    // Return the height of the object.
    return fMaxY - fMinY;
}
