/**************************************************
 *  File: GeometryEngine.h
 *  Author: Josh Fadaie
 *  Date: 06/26/15
 *  Project: Metropolis
 *
 *  Description: Parses
 *
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QObject>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QVector>
#include <QVector3D>

#include "singleton.h"
#include "objprocessor.h"
#include "mrglobals.h"


// Structure containing data necessary to render and transform an
// object. Object name is used for extra identification, and the
// center and radius values are used for proper ray casting/determining
// user interaction.
typedef struct ObjData {
    QOpenGLVertexArrayObject* pVAO;
    int                       iNumTriangles;
    QOpenGLBuffer             pVertexBuffer;
    QOpenGLBuffer             pNormalBuffer;
    QString                   qsObjName;
    QVector3D                 center3D;
    QVector4D                 transformedCenter4D;
    float                     fRadiusFromCenter;
    float                     fHeightY;
    MR::ObjectId              eID;
} ObjectData;


// VAO info is a collection of data necessary to define a Vertex
// array object for an object. Implemented here for compatibility
// with CreateVAO. Although the OBJProcessor data could be
// retrieved internally, VAOInfo might eventually be passed by the
// user directly and not through an OBJ file.
typedef struct VAOInfo {
    QVector<QVector3D>*             pfVertexList;
    QVector<QVector3D>*             pfNormalList;
    QString                         qsObjName;
    float                           fHeightY;
    QVector3D                       center3D;
    float                           fRadiusFromCenter;
    MR::ObjectId                    eID;
} VAOInfo;


class GeometryEngine : public QObject {
    Q_OBJECT
    friend class Singleton<GeometryEngine>;

public:
                            ~GeometryEngine();

    void                      initEngine(
                                          OBJProcessor& rProcessor,
                                          QOpenGLShaderProgram* pProgram );

    QString                   getObjectName( int iObjIndex );
    ObjectData&               getObjectData( int iObjIndex );
    QVector3D                 getObjectCenter(int iObjIndex );
    void                      drawObject( int iObjIndex );
    void                      drawLeftPillar(
                                        QOpenGLShaderProgram&   rProgram,
                                        const int               iHYPillar,
                                        const int               iHYWater,
                                        const QVector3D&        rTopClrVec );
    void                      drawCenterPillar(
                                        QOpenGLShaderProgram&   rProgram,
                                        const int               iHYPillar,
                                        const int               iHYWater,
                                        const QVector3D&        rTopClrVec );
    void                      drawRightPillar(
                                        QOpenGLShaderProgram&   rProgram,
                                        const int               iHYPillar,
                                        const int               iHYWater,
                                        const QVector3D&        rTopClrVec );
    void                      drawMiddleLighting(
                                        QOpenGLShaderProgram& rProgram,
                                        const QVector3D&      rBaseClrVec );
    void                      drawFountainStand(
                                         QOpenGLShaderProgram& rProgram,
                                         const QVector3D&      rBaseClrVec );

    int                       size() { return m_ObjectList.size(); }

    static GeometryEngine*    getEngine() {
                                GeometryEngine* pEngine =  Singleton<GeometryEngine>::GetInstance();
                                return pEngine; }

private:
    explicit                  GeometryEngine(QObject* pParent = 0);
    ObjectData*               objectData( MR::ObjectId eObjectId );
    void                      drawObject(
                                    QOpenGLShaderProgram& rProgram,
                                    MR::ObjectId          eObjectId,
                                    float                 fOffsetY = 0);
    void                      printVec3DList( QVector<QVector3D>* pVec3DList,
                                              const QString& rqsDebugTitle );
    void                      createVAO( VAOInfo& rInfo,
                                         QOpenGLShaderProgram* pProgram );
    void                      getProcessorVAOInfo( int iObjIndex,
                                                   OBJProcessor& rProcessor,
                                                   VAOInfo& rInfo );

    QVector<ObjectData*>            m_ObjectList;

    // TODO: possibly have variables to access and set these values.
    GLuint                          m_gluiPosAttribute;
    GLuint                          m_gluiNormalAttribute;
    GLuint                          m_gluiTexelAttribute;
    QOpenGLBuffer::UsagePattern     m_glBufferUsagePattern;

    // TODO: possibly have variables to access and set these values.
    // Default values are given as:
    //              inPosition3D
    //              inNormal3D
    //              inTexel3D
    QString                         m_qsPosAttrName;
    QString                         m_qsNormalAttrName;
    QString                         m_qsTexelAttrName;

signals:

public slots:


};


