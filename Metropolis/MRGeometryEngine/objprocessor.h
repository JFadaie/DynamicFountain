/***************************************
 *  File: objprocesser.h
 *  Author: Josh Fadaie
 *  Date: 06/26/2015
 *  Project: Metropolis
 *
 *  Description: Parses through a given wavefront file
 *  for the texel, normal and vertex data.
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#pragma once

#include <QObject>
#include <QVector>
#include <QVector3D>
#include <QString>

// Struct containing pointers to an Objects data.
// Objects are defined in the wavefront file as being
// prepended with a 'o' for "object" character.
typedef struct {
    QVector<QVector3D>* pVertexData;
    QVector<QVector3D>* pNormalData;
} ObjDataPtrs;


class OBJProcessor : public QObject {
    Q_OBJECT

public:
    explicit OBJProcessor(
                const QString& rqsFileName,
                QObject*       pParent = 0 );
    ~OBJProcessor();

    QVector<QVector3D>* getVerticesData(int iObjIndex );
    QVector<QVector3D>* getNormalsData( int iObjIndex );

    QString             getObjName( int iObjIndex ) {
                           Q_ASSERT( iObjIndex >= 0 &&
                                      iObjIndex < m_FountainObjNames.size() );
                           return m_FountainObjNames[iObjIndex]; }

    // Returns the number of objects retrieved by the OBJ File processor.
    int                 size() { return m_FountainObjBuf.size(); }
    float               calcObjectHeight( int iObjIndex );
private:
    // List of pointers to OBJ data. Each structure in the list contains pointers to
    // an objects vertex, and normal vectors.
    QVector<ObjDataPtrs> m_FountainObjBuf;

    QVector<QString>     m_FountainObjNames;

    // NOTE: Could have a starting index so that face vector
    // does not always read starting at index zero.
    // Face indices for objects will each be offset by the
    // number of vertices of the previous object.
    void getOBJLineData(
                         QString& rqsLine,
                         QVector<QVector3D>& rfPosVector,
                         QVector<QVector3D>& rfNormalsVector,
                         QVector<QVector2D>& rfTexelsVector,
                         QVector<QVector3D>& riFacesVector );

    void processVNOBJData(
                           QVector<QVector3D>& rfPosVector,
                           QVector<QVector3D>& rfNormalsVector,
                           QVector<QVector3D>& riFacesVector);
signals:

public slots:

};

