HEADERS += \
    MRGeometryEngine/geometryengine.h \
    MRGeometryEngine/objprocessor.h

SOURCES += \
    MRGeometryEngine/geometryengine.cpp \
    MRGeometryEngine/objprocessor.cpp
