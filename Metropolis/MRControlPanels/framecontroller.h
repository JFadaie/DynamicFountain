#pragma once

#include <QWidget>
#include <snapshot.h>

class AnimationViewer;

namespace Ui {
class FrameController;
}

class FrameController : public QWidget {
    Q_OBJECT

public:
    explicit FrameController(QWidget* pParent = 0);
    ~FrameController();

private:
    Ui::FrameController *ui;
    QVector<SnapShot* > m_SnapShotManager;
    int                 m_iCurrentIndex;
    AnimationViewer*    m_pAnimationViewer;

    void                addSnapShot();
    void                deleteSnapShot();
    void                demoAnimation();

signals:
    void            snapShotChanged( const SnapShot& rSnapShot );

    // Used to tell the main window to enable/ disable the control
    // panels. This ensures that the user does not modify the
    // snapshots during animation play.
    void            enablePanels( bool bEnabled );
public slots:
    void            setSnapShot( const SnapShot& rSnapShot );

private slots:
    void            onControlsChanged();
    void            onAnimationPlaying(
                                int iSnapShotIndex );
    void            onAnimationStopped( int iStoredIndex );
};


