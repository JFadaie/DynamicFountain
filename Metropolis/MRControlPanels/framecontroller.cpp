#include <QThread>

#include "framecontroller.h"
#include "ui_framecontroller.h"
#include "mrglobals.h"
#include "animationviewer.h"

enum ControlID {
    UNDEFINED_ID,
    SL_TIME,
    SP_SNAPSHOT,
    LB_MAXSLIDE,
    PB_DEMO,
    PB_STOP,
    PB_ADD,
    PB_DELETE
};


/***************************************************************
 * Function: FrameController::constructor
 *
 * Description: constructs.
 */

FrameController::FrameController(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FrameController)
{
    ui->setupUi(this);
    m_iCurrentIndex = 0;

    // Add the default number of snapshots.
    for ( int i = 0; i <= DEF_NUM_SNAPSHOTS; ++i ) {
        SnapShot* const pSnapShot = new SnapShot( this );
        m_SnapShotManager << pSnapShot;
    }

    // Set the ranges.
    ui->m_pslTime->setRange( 0, DEF_NUM_SNAPSHOTS );
    ui->m_pspSnapShot->setRange( 0, DEF_NUM_SNAPSHOTS );

    // Set the initial values of the controls to 0.
    ui->m_plbMaxSlide->setText( QString::number( DEF_NUM_SNAPSHOTS ));
    ui->m_pslTime->setValue( 0 );
    ui->m_pspSnapShot->setValue( 0 );

    // Update the viewport and controls console with the
    // current data.
    emit snapShotChanged( *m_SnapShotManager[0] );

    // Connect all of the controls to the signal handler.
    connect( ui->m_pslTime,     &QSlider::valueChanged,
             this,              &FrameController::onControlsChanged );
    connect( ui->m_pspSnapShot, SIGNAL( valueChanged( int ) ),
             this,              SLOT( onControlsChanged() ) );
    connect( ui->m_pbtDemo,     &QPushButton::clicked,
             this,              &FrameController::onControlsChanged );
    connect( ui->m_pbtStop,     &QPushButton::clicked,
             this,              &FrameController::onControlsChanged );
    connect( ui->m_pbtAdd,      &QPushButton::clicked,
             this,              &FrameController::onControlsChanged );
    connect( ui->m_pbtDelete,   &QPushButton::clicked,
             this,              &FrameController::onControlsChanged );
}


FrameController::~FrameController()
{
    delete ui;
    if ( m_pAnimationViewer != nullptr )
        m_pAnimationViewer->stopAnimation();
}

/***************************************************************
 * Function: FrameController::setSnapShot
 *
 * Description: Sets the current snapshot.
 */

void FrameController::setSnapShot( const SnapShot &rSnapShot )
{
   *m_SnapShotManager[m_iCurrentIndex] = rSnapShot;
    emit snapShotChanged(
                *m_SnapShotManager[m_iCurrentIndex] );
}


/***************************************************************
 * Function: FrameController::onControllerChanged
 *
 * Description: Handles events from the frame controller.
 * Emits a notification if the fountain snapshot
 * changes.
 */

void FrameController::onControlsChanged()
{
    QObject* const pSender = QObject::sender();

    struct Controls {
        QWidget*    pCtrl;
        ControlID   eCtrlId;
    };

    const Controls aControlIds[] = {
                        { ui->m_pslTime,        ControlID::SL_TIME },
                        { ui->m_pspSnapShot,    ControlID::SP_SNAPSHOT },
                        { ui->m_pbtDemo,        ControlID::PB_DEMO },
                        { ui->m_pbtStop,        ControlID::PB_STOP },
                        { ui->m_pbtAdd,         ControlID::PB_ADD },
                        { ui->m_pbtDelete,      ControlID::PB_DELETE } };

    ControlID eCtrlId = ControlID::UNDEFINED_ID;

    // Determine the Control Id associated with the sender.
    const int iSize = sizeof( aControlIds ) / sizeof( aControlIds[0]);
    for ( int i = 0; i < iSize; ++i ) {
        if ( pSender == aControlIds[i].pCtrl ) {
            eCtrlId = aControlIds[i].eCtrlId;
            break;
        }
    }

    switch( eCtrlId ) {
        case ControlID::SL_TIME: {
                const int iValue = ui->m_pslTime->value();

                // Setting the value will cause the spinbox to
                // emit a value changed signal if the value
                // is different than the previous.
                ui->m_pspSnapShot->setValue( iValue );
                m_iCurrentIndex = iValue;
            }
        break;
        case ControlID::SP_SNAPSHOT: {
                const int iValue = ui->m_pspSnapShot->value();

                // Setting the value will cause the slider to
                // emit a value changed signal if the value
                // is different than the previous.
                ui->m_pslTime->setValue( iValue );
                m_iCurrentIndex = iValue;

                // Update the current SnapShot.
                emit snapShotChanged(
                            *m_SnapShotManager[m_iCurrentIndex] );
            }
        break;
        case ControlID::PB_DEMO: {
                m_pAnimationViewer =
                                new AnimationViewer;

                // Initialized the animation to play.
                m_pAnimationViewer->setAnimation(
                                    m_iCurrentIndex,
                                    m_SnapShotManager );

                connect( m_pAnimationViewer, &AnimationViewer::drawSnapShot,
                         this,             &FrameController::onAnimationPlaying );
                connect( m_pAnimationViewer, &AnimationViewer::animationDone,
                         this,             &FrameController::onAnimationStopped );

                QThread* const pThread = new QThread;
                m_pAnimationViewer->moveToThread( pThread );

                // Play the animation when the animation viewer thread is started.
                connect( pThread,           &QThread::started,
                         m_pAnimationViewer,  &AnimationViewer::playAnimation );

                // Signals the thread to quit.
                connect( m_pAnimationViewer, &AnimationViewer::animationDone,
                         pThread,          &QThread::quit );

                // Causes the Animation Viewer to be deleted.
                connect( m_pAnimationViewer, &AnimationViewer::animationDone,
                         m_pAnimationViewer, &AnimationViewer::deleteLater );

                // Causes thread to be deleted only when it has fully shut down.
                connect( pThread, &QThread::finished,
                         pThread, &QThread::deleteLater );

                pThread->start();

                ui->m_pbtStop->setEnabled( true );
                ui->m_pbtDemo->setEnabled( false );
            }
            break;
        case ControlID::PB_STOP: {
            if ( m_pAnimationViewer != nullptr )
                m_pAnimationViewer->stopAnimation();

            ui->m_pbtStop->setEnabled( false );
            ui->m_pbtDemo->setEnabled( true );
        }
            break;
        case ControlID::PB_ADD: {
                SnapShot* const pSnapShot = new SnapShot( this );
                m_SnapShotManager.insert( m_iCurrentIndex, pSnapShot );
                ui->m_plbMaxSlide->setText( QString::number( m_SnapShotManager.size() - 1 ));

                // Set the ranges.
                ui->m_pslTime->setRange( 0, m_SnapShotManager.size() - 1 );
                ui->m_pspSnapShot->setRange( 0, m_SnapShotManager.size() - 1 );

                // Update the current SnapShot.
                emit snapShotChanged(
                            *m_SnapShotManager[m_iCurrentIndex] );
            }
        break;
        case ControlID::PB_DELETE: {
                if ( m_SnapShotManager.size() == 2 )
                    return; // Do not allow a size less than 2.

                SnapShot* const pCurrSnapShot = m_SnapShotManager.takeAt( m_iCurrentIndex );
                ui->m_plbMaxSlide->setText( QString::number( m_SnapShotManager.size() - 1) );

                delete pCurrSnapShot;

                // Update the current index.
                m_iCurrentIndex = m_iCurrentIndex % m_SnapShotManager.size();

                // Set the ranges.
                ui->m_pslTime->setRange( 0, m_SnapShotManager.size() - 1 );
                ui->m_pspSnapShot->setRange( 0, m_SnapShotManager.size() - 1 );

                // Update the current SnapShot.
                emit snapShotChanged(
                            *m_SnapShotManager[m_iCurrentIndex] );
            }
        break;
        default: break;
    }
}


void FrameController::onAnimationPlaying( int iSnapShotIndex )
{
    emit enablePanels( false );
    // Causes the snapshot changed signal to be emitted.
    ui->m_pslTime->setValue( iSnapShotIndex );
   // emit snapShotChanged(
   //             *m_SnapShotManager[iSnapShotIndex] );
}

void FrameController::onAnimationStopped( int iStoredIndex )
{
    emit enablePanels( true );
    ui->m_pslTime->setValue( iStoredIndex );

}
