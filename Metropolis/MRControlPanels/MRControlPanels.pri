HEADERS += \
    MRControlPanels/glmrwidget.h \
    MRControlPanels/watercontroller.h \
    $$PWD/pillarcontroller.h \
    $$PWD/lightcontroller.h \
    $$PWD/framecontroller.h \
    $$PWD/controlmanager.h

SOURCES += \
    MRControlPanels/glmrwidget.cpp \
    MRControlPanels/watercontroller.cpp \
    $$PWD/pillarcontroller.cpp \
    $$PWD/lightcontroller.cpp \
    $$PWD/framecontroller.cpp \
    $$PWD/controlmanager.cpp

FORMS += \
    MRControlPanels/watercontroller.ui \
    $$PWD/pillarcontroller.ui \
    $$PWD/lightcontroller.ui \
    $$PWD/framecontroller.ui
