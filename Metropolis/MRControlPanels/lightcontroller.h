/**************************************************
 *  File: LightController
 *  Author: Josh Fadaie
 *  Date: 06/26/15
 *  Project: Metropolis
 *
 *  Description: Widget that provides user controls
 *  for adjusting the color of the top, middle and
 *  bottom pillar columns.
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QWidget>
#include <QVector>
#include <QVector3D>
#include <QPushButton>
#include <QComboBox>

namespace Ui {
class LightController;
}

class LightController : public QWidget
{
    Q_OBJECT

public:
    explicit LightController(QWidget *parent = 0);
    ~LightController();

    QVector<QVector3D>      LEDColors() const {
                                    return m_LEDColors; }
    QVector<bool>           LEDStates() const {
                                    return m_LEDStates; }
    void                    setLEDColors(
                                    const QVector<QVector3D>& rLEDColors,
                                    const QVector<bool>&      rLEDStates );


private:
    Ui::LightController     *ui;

    // Note: Organized from bottom to top lights.
    QVector<QVector3D>      m_LEDColors;
    QVector<bool>           m_LEDStates;
    QVector<QPushButton*>   m_btStates;
    QVector<QComboBox* >    m_cxColors;
signals:
    void                    lightColorChanged();

private slots:
    void                    onControlsChanged();
};

