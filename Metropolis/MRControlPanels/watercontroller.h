/**************************************************
 *  File: WaterController
 *  Author: Josh Fadaie
 *  Date: 06/26/15
 *  Project: Metropolis
 *
 *  Description: Widget that provides user controls
 *  for adjusting the height of the left, center and
 *  right water columns.
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QWidget>
#include <QVector>

namespace Ui {
class WaterController;
}

class WaterController : public QWidget {
    Q_OBJECT

public:
    explicit WaterController( QWidget* pParent = 0 );
    ~WaterController();

    void                setWaterHeights( const QVector<int>& rHeightList );
    QVector<int>        waterHeights() const {
                                return m_WaterHeights; }

signals:
    void                waterHeightChanged();
private slots:
    void                onControlsChanged();
private:
    Ui::WaterController *ui;
    void                disconnectCtrlSignals();
    void                connectCtrlSignals();
    QVector<int>        m_WaterHeights;

};

