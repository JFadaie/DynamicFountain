/**************************************************
 *  File: PillarController
 *  Author: Josh Fadaie
 *  Date: 06/26/15
 *  Project: Metropolis
 *
 *  Description: Widget that provides user controls
 *  for adjusting the height of the left, center and
 *  right pillar columns.
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#pragma once

#include <QWidget>
#include <QVector>

namespace Ui {
class PillarController;
}

class PillarController : public QWidget
{
    Q_OBJECT

public:
    explicit PillarController(QWidget *parent = 0);
    ~PillarController();

    void                setPillarHeights( const QVector<int>& rHeightList );
    QVector<int>        pillarHeights() const {
                            return m_PillarHeights; }

private:
    Ui::PillarController *ui;
    QVector<int>        m_PillarHeights;
    void                disconnectCtrlSignals();
    void                connectCtrlSignals();

signals:
    void                pillarHeightChanged();
private slots:
    void                onControlsChanged();
};
