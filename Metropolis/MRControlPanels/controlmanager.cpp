#include <QVBoxLayout>

#include "controlmanager.h"

#define        SPACING      7

enum ControlID {
    UNDEFINED_ID,
    WATER_CTRL,
    PILLAR_CTRL,
    LIGHT_CTRL
};


/***************************************************************
 * Function: ControlManager::constructor/destructor
 *
 * Description: initialize/destroy.
 */

ControlManager::ControlManager(QWidget *parent) : QWidget(parent)
{
    initLayout();

    // Prime the control panels: lights, water, pillars.
    setSnapShot( m_CurrSnapShot );
}

ControlManager::~ControlManager()
{

}

/***************************************************************
 * Function: ControlManager::initLayout
 *
 * Description: Initializes the layout. Adds the water, pillar, and
 * lights control widgets.
 */

void ControlManager::initLayout()
{
    QVBoxLayout* const pVBoxLayout = new QVBoxLayout;
    pVBoxLayout->setContentsMargins( 0, 0, 0, 0 );
    pVBoxLayout->setSpacing( SPACING );

    m_pWaterCtrl    = new WaterController;
    m_pPillarCtrl   = new PillarController;
    m_pLightCtrl    = new LightController;

    pVBoxLayout->addWidget( m_pWaterCtrl );
    pVBoxLayout->addWidget( m_pPillarCtrl );
    pVBoxLayout->addWidget( m_pLightCtrl );

    setLayout( pVBoxLayout );


    connect( m_pWaterCtrl, &WaterController::waterHeightChanged,
             this,         &ControlManager::onControllerChanged );
    connect( m_pPillarCtrl, &PillarController::pillarHeightChanged,
             this,         &ControlManager::onControllerChanged );
    connect( m_pLightCtrl, &LightController::lightColorChanged,
             this,         &ControlManager::onControllerChanged );
}


/***************************************************************
 * Function: ControlManager::onControllerChanged
 *
 * Description: Handles events from the control panels under
 * management. Emits a notification if the fountain snapshot
 * changes.
 */

void ControlManager::onControllerChanged()
{
    QObject* const pSender = QObject::sender();

    struct Controls {
        QWidget*    pCtrl;
        ControlID   eCtrlId;
    };

    const Controls aControlIds[] = {
                        { m_pWaterCtrl,    ControlID::WATER_CTRL },
                        { m_pPillarCtrl,   ControlID::PILLAR_CTRL },
                        { m_pLightCtrl,    ControlID::LIGHT_CTRL } };

    ControlID eCtrlId = ControlID::UNDEFINED_ID;

    // Determine the Control Id associated with the sender.
    const int iSize = sizeof( aControlIds ) / sizeof( aControlIds[0]);
    for ( int i = 0; i < iSize; ++i ) {
        if ( pSender == aControlIds[i].pCtrl ) {
            eCtrlId = aControlIds[i].eCtrlId;
            break;
        }
    }

    switch( eCtrlId ) {
        case ControlID::WATER_CTRL: {
                const QVector<int> waterHeights =
                        m_pWaterCtrl->waterHeights();
                m_CurrSnapShot.setWaterHeights( waterHeights );
                snapShotChanged( m_CurrSnapShot );
            }
        break;
        case ControlID::PILLAR_CTRL: {
                const QVector<int> pillarHeights =
                        m_pPillarCtrl->pillarHeights();
                m_CurrSnapShot.setPillarHeights( pillarHeights );
                snapShotChanged( m_CurrSnapShot );
            }
        break;
        case ControlID::LIGHT_CTRL: {
                const QVector<QVector3D> LEDColors =
                        m_pLightCtrl->LEDColors();
                const QVector<bool>      LEDStates =
                        m_pLightCtrl->LEDStates();
                m_CurrSnapShot.setLEDColors(
                                            LEDColors,
                                            LEDStates );
                snapShotChanged( m_CurrSnapShot );
            }
        break;
        default: break;
    }
}


/***************************************************************
 * Function: ControlManager::setSnapShot
 *
 * Description: Sets the current SnapShot, and provides
 * data to the water, pillar and color control panels.
 */

void ControlManager::setSnapShot( const SnapShot &rSnapShot )
{
    m_CurrSnapShot = rSnapShot;

    m_pWaterCtrl->setWaterHeights( rSnapShot.waterHeights() );
    m_pPillarCtrl->setPillarHeights( rSnapShot.pillarHeights() );
    m_pLightCtrl->setLEDColors(
                               rSnapShot.LEDColors(),
                               rSnapShot.LEDStates() );
}
