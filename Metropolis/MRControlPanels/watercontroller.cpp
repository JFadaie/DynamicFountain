/**************************************************
 *  File: WaterController
 *  Author: Josh Fadaie
 *  Date: 06/26/15
 *  Project: Metropolis
 *
 *  Description: Widget that provides user controls
 *  for adjusting the height of the left, center and
 *  right water columns.
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#include <QFile>
#include <QSpinBox>
#include "watercontroller.h"
#include "ui_watercontroller.h"
#include "mrglobals.h"



enum ControlID {
    UNDEFINED_ID,
    SP_LEFT,
    SP_CENTER,
    SP_RIGHT,
    SL_LEFT,
    SL_CENTER,
    SL_RIGHT
};


/***************************************************************
 * Function: WaterController::Contstructor
 *
 * Description: constructor
 */

WaterController::WaterController(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WaterController)
{
    // Set the style sheet for the sliders.
    ui->setupUi(this);

    ui->m_pslWaterLeft->setRange( WATER_MIN_HEIGHT, WATER_MAX_HEIGHT );
    ui->m_pslWaterCenter->setRange(
                                WATER_MIN_HEIGHT, WATER_MAX_HEIGHT );
    ui->m_pslWaterRight->setRange(
                                WATER_MIN_HEIGHT, WATER_MAX_HEIGHT );
    ui->m_pspWaterLeft->setRange(
                                WATER_MIN_HEIGHT, WATER_MAX_HEIGHT );
    ui->m_pspWaterCenter->setRange(
                                WATER_MIN_HEIGHT, WATER_MAX_HEIGHT );
    ui->m_pspWaterRight->setRange(
                                WATER_MIN_HEIGHT, WATER_MAX_HEIGHT );


    m_WaterHeights.reserve( 3 );
    m_WaterHeights << 0 << 0 << 0;
    setWaterHeights( m_WaterHeights );

    connectCtrlSignals();
}

WaterController::~WaterController()
{
    delete ui;
}


/***************************************************************
 * Function: WaterController::setWaterHeights
 *
 * Description: Sets the height of the water, and updates the
 * control elements. This does not cause the controlsChanged
 * signal to be emitted.
 */

void WaterController::setWaterHeights( const QVector<int> &rHeightList )
{
    m_WaterHeights = rHeightList;

    bool bChanged = false;

    // The values of the spinbox and slider are always in updated to
    // be in sync. Theerefore, only one the spin boxes or sliders
    // need to be checked.
    if ( ui->m_pspWaterLeft->value()   != m_WaterHeights[0] ||
         ui->m_pspWaterCenter->value() != m_WaterHeights[1] ||
         ui->m_pspWaterRight->value()  != m_WaterHeights[2] )
        bChanged = true;

    if ( bChanged ) {
        // Disconnect the update notification.
        disconnectCtrlSignals();

        // Set the new value in the spin box and combo box.
        ui->m_pspWaterLeft->setValue( m_WaterHeights[0] );
        ui->m_pspWaterCenter->setValue( m_WaterHeights[1] );
        ui->m_pspWaterRight->setValue( m_WaterHeights[2] );

        ui->m_pslWaterLeft->setValue( m_WaterHeights[0] );
        ui->m_pslWaterCenter->setValue( m_WaterHeights[1] );
        ui->m_pslWaterRight->setValue( m_WaterHeights[2] );

        // Reconnect the update notification.
        connectCtrlSignals();
    }
}

/***************************************************************
 * Function: WaterController::disconnectCtrlSignals
 *           WaterController::connectCtrlSignals
 *
 * Description: These functions connect and disconnect the signals
 * emitted by the control elements (slider and spin box) from the
 * connected controls changed slot.
 */

void WaterController::disconnectCtrlSignals()
{
    // Disconnect the spin box signals.
    disconnect( ui->m_pspWaterLeft,   SIGNAL( valueChanged(int ) ),
                this,                 SLOT( onControlsChanged() ) );
    disconnect( ui->m_pspWaterCenter, SIGNAL( valueChanged(int ) ),
                this,                 SLOT( onControlsChanged() ) );
    disconnect( ui->m_pspWaterRight, SIGNAL( valueChanged(int ) ),
                this,                 SLOT( onControlsChanged() ) );

    // Disconnect the slider signals.
    disconnect( ui->m_pslWaterLeft,   &QSlider::valueChanged,
                this,                 &WaterController::onControlsChanged );
    disconnect( ui->m_pslWaterCenter,   &QSlider::valueChanged,
                this,                 &WaterController::onControlsChanged );
    disconnect( ui->m_pslWaterRight,   &QSlider::valueChanged,
                this,                 &WaterController::onControlsChanged );
}

void WaterController::connectCtrlSignals()
{
    connect( ui->m_pspWaterLeft,    SIGNAL( valueChanged(int ) ),
             this,                  SLOT( onControlsChanged() ) );
    connect( ui->m_pspWaterCenter,  SIGNAL( valueChanged(int ) ),
             this,                  SLOT( onControlsChanged() ) );
    connect( ui->m_pspWaterRight,  SIGNAL( valueChanged(int ) ),
             this,                  SLOT( onControlsChanged() ) );

    connect( ui->m_pslWaterLeft, &QSlider::valueChanged,
             this,               &WaterController::onControlsChanged );
    connect( ui->m_pslWaterCenter, &QSlider::valueChanged,
             this,               &WaterController::onControlsChanged );
    connect( ui->m_pslWaterRight, &QSlider::valueChanged,
             this,               &WaterController::onControlsChanged );
}


/***************************************************************
 * Function: WaterController::onControlsChanged
 *
 * Description: Updates the control widgets with the correct
 * height values. Notifies observers that a water column height
 * has changed.
 */

void WaterController::onControlsChanged()
{
    QObject* const pSender = QObject::sender();

    struct Controls {
        QWidget*    pCtrl;
        ControlID   eCtrlId;
    };

    const Controls aControlIds[] = {
                        { ui->m_pspWaterLeft,     ControlID::SP_LEFT },
                        { ui->m_pspWaterCenter,   ControlID::SP_CENTER },
                        { ui->m_pspWaterRight,    ControlID::SP_RIGHT },
                        { ui->m_pslWaterLeft,     ControlID::SL_LEFT },
                        { ui->m_pslWaterCenter,   ControlID::SL_CENTER },
                        { ui->m_pslWaterRight,    ControlID::SL_RIGHT } };

    ControlID eCtrlId = ControlID::UNDEFINED_ID;

    // Determine the Control Id associated with the sender.
    const int iSize = sizeof( aControlIds ) / sizeof( aControlIds[0]);
    for ( int i = 0; i < iSize; ++i ) {
        if ( pSender == aControlIds[i].pCtrl ) {
            eCtrlId = aControlIds[i].eCtrlId;
            break;
        }
    }

    switch( eCtrlId ) {
        case ControlID::SP_LEFT: {
                ui->m_pslWaterLeft->setValue(
                            ui->m_pspWaterLeft->value() );
            }
        break;
        case ControlID::SP_CENTER: {
                ui->m_pslWaterCenter->setValue(
                            ui->m_pspWaterCenter->value() );
            }
        break;
        case ControlID::SP_RIGHT: {
                 ui->m_pslWaterRight->setValue(
                            ui->m_pspWaterRight->value() );
            }
        break;
        case ControlID::SL_LEFT: {
                const int iHeight = ui->m_pslWaterLeft->value();
                ui->m_pspWaterLeft->setValue( iHeight );
                m_WaterHeights[0] = iHeight;
                emit waterHeightChanged();
            }
        break;
        case ControlID::SL_CENTER: {
                const int iHeight = ui->m_pslWaterCenter->value();
                ui->m_pspWaterCenter->setValue( iHeight );
                m_WaterHeights[1] = iHeight;
                emit waterHeightChanged();
            }
        break;
        case ControlID::SL_RIGHT: {
                const int iHeight = ui->m_pslWaterRight->value();
                ui->m_pspWaterRight->setValue( iHeight );
                m_WaterHeights[2] = iHeight;
                emit waterHeightChanged();
            }
        break;
        default: break;
    }
}
