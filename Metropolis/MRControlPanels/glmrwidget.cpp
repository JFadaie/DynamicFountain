/***************************************
 *  File: GLMRWidget
 *  Author: Josh Fadaie
 *  Date: 06/26/15
 *  Project:Metropolis
 *
 *  Description: The main widget that displays the fountain providing
 *  the user with visual feedback on the resulting structure. The
 *  widget provides the ability to rotate, zoom, and updated the
 *  fountain object.
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/

#include <QMouseEvent>
#include <QPoint>
#include <QKeyEvent>

#include "glmrwidget.h"
#include "geometryengine.h"
#include "mrglobals.h"

#define     OBJ_FILE    "/Users/joshfadaie/BlenderFiled/FountainBase_V5.obj"


/***************************************************************
 * Function: GLMRWidget::constructor
 *
 * Description: Sets the initial rotation angle to XYZ <0,0,0>
 */

GLMRWidget::GLMRWidget( QWidget* pParent ) :
    QOpenGLWidget( pParent )
{
    m_iXRot = 0;
    m_iYRot = 0;
    m_iZRot = 0;

    setFocusPolicy( Qt::ClickFocus );
}


/***************************************************************
 * Function: GLMRWidget::initializeGL
 *
 * Description: Initializes necessary opengl parameters, initializes the
 * shader program, and parses the normal/vertex data for each object.
 *
 */

void GLMRWidget::initializeGL()
{
    initializeOpenGLFunctions();

    // Convenience function for specifying the clearing color to OpenGL.
    // Calls glClearColor (in RGBA mode) or glClearIndex (in color-index
    // mode) with the color c. Applies to this widgets GL context.
    glClearColor( 0.88, 0.88, 0.88, 1);

    // Enables depth comparisions and updating the depth buffer.
    glEnable(GL_DEPTH_TEST);

    // Culls polygons based on their winding in window coords
    glEnable(GL_CULL_FACE);

    // Smooth shading, the default, causes the computed colors of vertices
    // to be interpolated as the primitives are rasterized.
    glShadeModel(GL_SMOOTH);

    // Create shader program, add shaders, bind to the context, and link.
    m_pProgram = new QOpenGLShaderProgram( this );
    m_pProgram->addShaderFromSourceFile(QOpenGLShader::Vertex,  ":/qrc/vertexShader.vert" );
    m_pProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/qrc/fragmentShader.frag" );
    m_pProgram->bind();
    m_pProgram->link();

    // Process the blender file for object data.
    QString qsOBJFile( OBJ_FILE );
    OBJProcessor Processor( qsOBJFile );

    // Store the blender file data inside the geometry engine
    GeometryEngine* pEngine = GeometryEngine::getEngine();
    pEngine->initEngine( Processor, m_pProgram );

    // Initialize the current snap shot.
    m_CurrSnapShot = SnapShot();
}


/***************************************************************
 * Function: GLMRWidget::paintGL
 *
 * Description: Redraws the object on the screen. This function
 * draws a rotated, scaled, and dynamic version of the fountain.
 * The fountains LED colors, height of pillars, and height of
 * water columns are drawn here.
 */

void GLMRWidget::paintGL()
{
    // Clears indicated buffer's to preset values. Sets the bitplane area of the window to
    // values previously selected by glClearColor, glClearDepth, glClearStencil.
    // GL_COLOR_BUFFER_BIT: indicates the buffers currently enabled for color writing.
    // GL_DEPTH_BUFFER_BIT: indicates the depth buffer.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GeometryEngine* pEngine = GeometryEngine::getEngine();

    m_pProgram->bind();
    const QVector3D FountainCenter3D = QVector3D( 0.0, -8.0, -5.0 );
    // Do Transformations.
    // Begin model matrix transformations.
    m_ModelMatrix = QMatrix4x4();
    m_ModelMatrix.translate( FountainCenter3D );
    m_ModelMatrix.rotate(m_iXRot / 18.0, 1.0, 0.0, 0.0);
    m_ModelMatrix.rotate(m_iYRot / 18.0, 0.0, 1.0, 0.0);
    m_ModelMatrix.rotate(m_iZRot / 18.0, 0.0, 0.0, 1.0);

    // Begin view matrix transformations.
    const QVector3D eyePos3D = QVector3D( 0, 0, -16.0 );
    const QVector3D upDir3D = QVector3D( 0, 1, 0 );
    m_ViewMatrix = QMatrix4x4();
    m_ViewMatrix.lookAt( eyePos3D, FountainCenter3D, upDir3D );

    // Set uniform values
    m_pProgram->setUniformValue( "uMMat", m_ModelMatrix );
    m_pProgram->setUniformValue( "uVMat", m_ViewMatrix );
    m_pProgram->setUniformValue( "uPMat", m_ProjectionMatrix );

    // Grabs the current snapshots fountain information.
    const QVector<QVector3D> colors = m_CurrSnapShot.trueLEDColors();
    const QVector<int>       pH     = m_CurrSnapShot.pillarHeights();
    const QVector<int>       wH     = m_CurrSnapShot.waterHeights();

    // Draws the pillars, fountain stand, and lights. The color of the lights,
    // height of the pillars and height of the water can be modified.
    // NOTE: The height of the pillars is offset by 1 to indicate to the
    // user that the lowest pillar settings still provides some height.
    // However, a height of '1' indicates that the 'middle pillar object'
    // is not displayed. Therefore, the height multiplier needs to be
    // subtracted by '1' to cause this object not to be drawn, i.e.
    // zero instances of the 'middle pillar object'.
    pEngine->drawFountainStand( *m_pProgram, colors[0] );
    pEngine->drawLeftPillar( *m_pProgram, pH[0] - 1, wH[0], colors[1] );
    pEngine->drawCenterPillar( *m_pProgram, pH[1] - 1, wH[1], colors[1] );
    pEngine->drawRightPillar( *m_pProgram, pH[2] - 1, wH[2], colors[1] );
    pEngine->drawMiddleLighting( *m_pProgram, colors[2] );


#if 0 // Working.
    // Draw the geometry.
    QString qsObjectName;
    for ( int iIndex = 0; iIndex < pEngine->size(); ++iIndex ) {

        qsObjectName = pEngine->getObjectName( iIndex );
        int iObjClrType = (int ) MR::ObjectColorType( qsObjectName );
        m_pProgram->setUniformValue( "uiObjClrType", iObjClrType );
       // m_pProgram->setUniformValue( "uiObjState", eState );
        pEngine->drawObject( iIndex );
    }
#endif
}


/***************************************************************
 * Function: GLMRWidget::resizeGL
 *
 * Description: Sets up the OpenGL viewport, projection, etc.
 * Gets called whenever the widget has been resized (and also when
 * it is shown for the first time because all newly created
 * widgets get a resize event automatically
 */

void GLMRWidget::resizeGL(int w, int h)
{
    m_ViewportSize = QSize( w, h );

    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    const qreal zNear = 0.1, zFar = 100, fov = 65.0;

    // Reset projection
    m_ProjectionMatrix.setToIdentity();

    // Set perspective projection
    m_ProjectionMatrix.perspective(fov, aspect, zNear, zFar);

    update();
}


/***************************************************************
 * Function: GLMRWidget::mousePressEvent
 *
 * Description: Stores the position of the mouse press.
 */

void GLMRWidget::mousePressEvent(QMouseEvent* pEvent)
{
    m_qptLastPos = pEvent->pos();
}


/***************************************************************
 * Function: GLMRWidget::mouseMoveEvent
 *
 * Description: Rotates the fountain using current mouse movement
 * in relation to the previously stored mouse press coordinates
 */

void GLMRWidget::mouseMoveEvent(QMouseEvent* pEvent)
{
    int dx = pEvent->x() - m_qptLastPos.x();
    int dy = pEvent->y() - m_qptLastPos.y();

    if (pEvent->buttons() & Qt::LeftButton) {
        setXRotation(m_iXRot + 18 * dy);
        setYRotation(m_iYRot + 18 * dx);
    } else if (pEvent->buttons() & Qt::RightButton) {
        setXRotation(m_iXRot + 18 * dy);
        setZRotation(m_iZRot + 18 * dx);
    }

    m_qptLastPos = pEvent->pos();
}

/***************************************************************
 * Function: GLMRWidget::setXRotation
 *           GLMRWidget::setYRotation
 *           GLMRWidget::setZRotation
 *
 * Description: Sets the rotation angle and updates the screen.
 */

void GLMRWidget::setXRotation(int iAngle)
{
    if (iAngle != m_iXRot) {
        m_iXRot = iAngle;
        update();
    }
}

void GLMRWidget::setYRotation(int iAngle)
{
    if (iAngle != m_iYRot) {
        m_iYRot = iAngle;
        update();
    }
}

void GLMRWidget::setZRotation(int iAngle)
{
    if (iAngle != m_iZRot) {
        m_iZRot = iAngle;
        update();
    }
}


/***************************************************************
 * Function: GLMRWidget::setSnapShot
 *
 * Description: Sets the new Snapshot, and updates the screen.
 */

void GLMRWidget::setSnapShot( const SnapShot& rSnapshot )
{
    m_CurrSnapShot = rSnapshot;
    update();
}
