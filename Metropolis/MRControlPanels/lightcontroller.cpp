/**************************************************
 *  File: LightController
 *  Author: Josh Fadaie
 *  Date: 06/26/15
 *  Project: Metropolis
 *
 *  Description: Widget that provides user controls
 *  for adjusting the color of the top, middle and
 *  bottom pillar columns.
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#include "lightcontroller.h"
#include "ui_lightcontroller.h"
#include "mrglobals.h"

#define     LIGHT_OFF       "OFF"
#define     LIGHT_ON        "ON"

enum ControlID {
    UNDEFINED_ID,
    CX_BOTTOM,
    CX_MIDDLE,
    CX_TOP,
    PB_BOTTOM,
    PB_MIDDLE,
    PB_TOP,
};

/***************************************************************
 * Function: LightController::Contstructor
 *
 * Description: constructor
 */

LightController::LightController(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LightController)
{
    ui->setupUi(this);

    m_LEDColors
            << MR::ColorVector( "GhostWhite")
            << MR::ColorVector( "GhostWhite")
            << MR::ColorVector( "GhostWhite");
    m_LEDStates << false << false << false;

    // Initially indicate that the LEDs are OFF.
    ui->m_pbtBottom->setText( LIGHT_OFF );
    ui->m_pbtMiddle->setText( LIGHT_OFF );
    ui->m_pbtTop->setText( LIGHT_OFF );

    // Add the String Colors to the combo boxes.
    ui->m_pcxBottomLight->addItems( MR::GetColors() );
    ui->m_pcxMiddleLight->addItems( MR::GetColors() );
    ui->m_pcxTopLight->addItems( MR::GetColors() );

    // Set the LED colors.
    ui->m_pcxBottomLight->setCurrentText( "GhostWhite" );
    ui->m_pcxMiddleLight->setCurrentText( "GhostWhite" );
    ui->m_pcxTopLight->setCurrentText( "GhostWhite" );

    m_btStates << ui->m_pbtBottom << ui->m_pbtMiddle << ui->m_pbtTop;
    m_cxColors
            << ui->m_pcxBottomLight
            << ui->m_pcxMiddleLight
            << ui->m_pcxTopLight;

    connect( ui->m_pbtBottom, &QPushButton::clicked,
             this,            &LightController::onControlsChanged );
    connect( ui->m_pbtMiddle, &QPushButton::clicked,
             this,            &LightController::onControlsChanged );
    connect( ui->m_pbtTop,    &QPushButton::clicked,
             this,            &LightController::onControlsChanged );
    connect( ui->m_pcxBottomLight, SIGNAL(currentIndexChanged(int)),
             this,                 SLOT( onControlsChanged()) );
    connect( ui->m_pcxMiddleLight, SIGNAL(currentIndexChanged(int)),
             this,                 SLOT( onControlsChanged()) );
    connect( ui->m_pcxTopLight, SIGNAL(currentIndexChanged(int)),
             this,              SLOT( onControlsChanged()));
}


LightController::~LightController()
{
    delete ui;
}

/***************************************************************
 * Function: PillarController::setLEDColors
 *
 * Description: Sets the current LED states and colors.
 * The comboboxes and pushbuttons are updated to reflect this.
 *
 * Note: The LEDColors and States are organized from bottom to
 * top.
 */

void LightController::setLEDColors(
                        const QVector<QVector3D>& rLEDColors,
                        const QVector<bool>&      rLEDStates )
{
    m_LEDStates = rLEDStates;
    m_LEDColors = rLEDColors;

    // Determine the state of the LEDs.
    for ( int i = 0; i < m_LEDStates.size(); ++i ) {
        if ( m_LEDStates[i] )       // ON.
            m_btStates[i]->setText( LIGHT_ON );
        else
            m_btStates[i]->setText( LIGHT_OFF );
    }

    // Loop over all of the colors. Use the first combo box's size.
    for ( int iClrIndex = 0; iClrIndex < m_cxColors[0]->count(); ++iClrIndex ) {
        const QVector3D currColor3D =
                MR::ColorVector( m_cxColors[0]->itemText( iClrIndex) );
        // Determine if the color matches the combo box.
        for ( int iCXIndex = 0; iCXIndex < m_LEDColors.size(); ++iCXIndex ) {
            if ( currColor3D == m_LEDColors[iCXIndex] )       // ON.
                m_cxColors[iCXIndex]->setCurrentIndex( iClrIndex );
        }
    }
}


/***************************************************************
 * Function: PillarController::setLEDColors
 *
 * Description: Updates the control widgets with the correct LED
 * color and state values. Notifies observers if the LEDs status
 * changes.
 */

void LightController::onControlsChanged()
{
    QObject* const pSender = QObject::sender();

    struct Controls {
        QWidget*    pCtrl;
        ControlID   eCtrlId;
    };


    const Controls aControlIds[] = {
                        { ui->m_pcxBottomLight,     ControlID::CX_BOTTOM },
                        { ui->m_pcxMiddleLight,     ControlID::CX_MIDDLE },
                        { ui->m_pcxTopLight,        ControlID::CX_TOP },
                        { ui->m_pbtBottom,          ControlID::PB_BOTTOM },
                        { ui->m_pbtMiddle,          ControlID::PB_MIDDLE },
                        { ui->m_pbtTop,             ControlID::PB_TOP } };

    ControlID eCtrlId = ControlID::UNDEFINED_ID;

    // Determine the Control Id associated with the sender.
    const int iSize = sizeof( aControlIds ) / sizeof( aControlIds[0]);
    for ( int i = 0; i < iSize; ++i ) {
        if ( pSender == aControlIds[i].pCtrl ) {
            eCtrlId = aControlIds[i].eCtrlId;
            break;
        }
    }

    switch( eCtrlId ) {
        case ControlID::CX_BOTTOM: {
                m_LEDColors[0] =
                        MR::ColorVector( ui->m_pcxBottomLight->currentText() );
                emit lightColorChanged();
            }
        break;
        case ControlID::CX_MIDDLE: {
                m_LEDColors[1] =
                        MR::ColorVector( ui->m_pcxMiddleLight->currentText() );
                emit lightColorChanged();
            }
        break;
        case ControlID::CX_TOP: {
                m_LEDColors[2] =
                        MR::ColorVector( ui->m_pcxTopLight->currentText() );
                emit lightColorChanged();
            }
        break;
        case ControlID::PB_BOTTOM: {
                // Toggle the pushbutton text state.
                if ( ui->m_pbtBottom->text() == LIGHT_OFF )
                    ui->m_pbtBottom->setText( LIGHT_ON );
                else
                    ui->m_pbtBottom->setText( LIGHT_OFF );
                m_LEDStates[0] = ui->m_pbtBottom->text() == LIGHT_ON;
                emit lightColorChanged();
            }
        break;
        case ControlID::PB_MIDDLE: {
                // Toggle the pushbutton text state.
                if ( ui->m_pbtMiddle->text() == LIGHT_OFF )
                    ui->m_pbtMiddle->setText( LIGHT_ON );
                else
                    ui->m_pbtMiddle->setText( LIGHT_OFF );
                m_LEDStates[1] = ui->m_pbtMiddle->text() == LIGHT_ON;
                emit lightColorChanged();
            }
        break;
        case ControlID::PB_TOP: {
                // Toggle the pushbutton text state.
                if ( ui->m_pbtTop->text() == LIGHT_OFF )
                    ui->m_pbtTop->setText( LIGHT_ON );
                else
                    ui->m_pbtTop->setText( LIGHT_OFF );
                m_LEDStates[2] = ui->m_pbtTop->text() == LIGHT_ON;
                emit lightColorChanged();
            }
        break;
        default: break;
    }
}
