/**************************************************
 *  File: PillarController
 *  Author: Josh Fadaie
 *  Date: 06/26/15
 *  Project: Metropolis
 *
 *  Description: Widget that provides user controls
 *  for adjusting the height of the left, center and
 *  right pillar columns.
 *
 *  Notes: None
 *
 *  References: None
 *
 **************************************************/

#include "pillarcontroller.h"
#include "ui_pillarcontroller.h"
#include "mrglobals.h"

enum ControlID {
    UNDEFINED_ID,
    SP_LEFT,
    SP_CENTER,
    SP_RIGHT,
    SL_LEFT,
    SL_CENTER,
    SL_RIGHT
};


/***************************************************************
 * Function: PillarController::Contstructor
 *
 * Description: constructor
 */

PillarController::PillarController(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PillarController)
{
    ui->setupUi(this);

    ui->m_pslPillarLeft->setRange( PILLAR_MIN_HEIGHT, PILLAR_MAX_HEIGHT );
    ui->m_pslPillarCenter->setRange(
                                PILLAR_MIN_HEIGHT, PILLAR_MAX_HEIGHT );
    ui->m_pslPillarRight->setRange(
                                PILLAR_MIN_HEIGHT, PILLAR_MAX_HEIGHT );
    ui->m_pspPillarLeft->setRange(
                                PILLAR_MIN_HEIGHT, PILLAR_MAX_HEIGHT );
    ui->m_pspPillarCenter->setRange(
                                PILLAR_MIN_HEIGHT, PILLAR_MAX_HEIGHT );
    ui->m_pspPillarRight->setRange(
                                PILLAR_MIN_HEIGHT, PILLAR_MAX_HEIGHT );

    m_PillarHeights.reserve( 3 );
    m_PillarHeights << 1 << 1 << 1;
    setPillarHeights( m_PillarHeights );

    connectCtrlSignals();
}

PillarController::~PillarController()
{
    delete ui;
}


/***************************************************************
 * Function: PillarController::setPillarHeights
 *
 * Description: Sets the height of the pillars, and updates the
 * control elements. This does not cause the controlsChanged
 * signal to be emitted.
 */

void PillarController::setPillarHeights( const QVector<int> &rHeightList )
{
    m_PillarHeights = rHeightList;

    bool bChanged = false;

    // The values of the spinbox and slider are always in updated to
    // be in sync. Theerefore, only one the spin boxes or sliders
    // need to be checked.
    if ( ui->m_pspPillarLeft->value()   != m_PillarHeights[0] ||
         ui->m_pspPillarCenter->value() != m_PillarHeights[1] ||
         ui->m_pspPillarRight->value()  != m_PillarHeights[2] )
        bChanged = true;

    if ( bChanged ) {
        // Disconnect the update notification.
        disconnectCtrlSignals();

        // Set the new value in the spin box and combo box.
        ui->m_pspPillarLeft->setValue( m_PillarHeights[0] );
        ui->m_pspPillarCenter->setValue( m_PillarHeights[1] );
        ui->m_pspPillarRight->setValue( m_PillarHeights[2] );

        ui->m_pslPillarLeft->setValue( m_PillarHeights[0] );
        ui->m_pslPillarCenter->setValue( m_PillarHeights[1] );
        ui->m_pslPillarRight->setValue( m_PillarHeights[2] );

        // Reconnect the update notification.
        connectCtrlSignals();
    }
}

/***************************************************************
 * Function: PillarController::disconnectCtrlSignals
 *           PillarController::connectCtrlSignals
 *
 * Description: These functions connect and disconnect the signals
 * emitted by the control elements (slider and spin box) from the
 * connected controls changed slot.
 */

void PillarController::disconnectCtrlSignals()
{
    // Disconnect the spin box signals.
    disconnect( ui->m_pspPillarLeft,   SIGNAL( valueChanged(int ) ),
                this,                 SLOT( onControlsChanged() ) );
    disconnect( ui->m_pspPillarCenter, SIGNAL( valueChanged(int ) ),
                this,                 SLOT( onControlsChanged() ) );
    disconnect( ui->m_pspPillarRight, SIGNAL( valueChanged(int ) ),
                this,                 SLOT( onControlsChanged() ) );

    // Disconnect the slider signals.
    disconnect( ui->m_pslPillarLeft,   &QSlider::valueChanged,
                this,                 &PillarController::onControlsChanged );
    disconnect( ui->m_pslPillarCenter,   &QSlider::valueChanged,
                this,                 &PillarController::onControlsChanged );
    disconnect( ui->m_pslPillarRight,   &QSlider::valueChanged,
                this,                 &PillarController::onControlsChanged );
}

void PillarController::connectCtrlSignals()
{
    connect( ui->m_pspPillarLeft,    SIGNAL( valueChanged(int ) ),
             this,                  SLOT( onControlsChanged() ) );
    connect( ui->m_pspPillarCenter,  SIGNAL( valueChanged(int ) ),
             this,                  SLOT( onControlsChanged() ) );
    connect( ui->m_pspPillarRight,  SIGNAL( valueChanged(int ) ),
             this,                  SLOT( onControlsChanged() ) );

    connect( ui->m_pslPillarLeft, &QSlider::valueChanged,
             this,               &PillarController::onControlsChanged );
    connect( ui->m_pslPillarCenter, &QSlider::valueChanged,
             this,               &PillarController::onControlsChanged );
    connect( ui->m_pslPillarRight, &QSlider::valueChanged,
             this,               &PillarController::onControlsChanged );
}


/***************************************************************
 * Function: PillarController::onControlsChanged
 *
 * Description: Updates the control widgets with the correct
 * height values. Notifies observers that a Pillar column height
 * has changed.
 */

void PillarController::onControlsChanged()
{
    QObject* const pSender = QObject::sender();

    struct Controls {
        QWidget*    pCtrl;
        ControlID   eCtrlId;
    };

    const Controls aControlIds[] = {
                        { ui->m_pspPillarLeft,     ControlID::SP_LEFT },
                        { ui->m_pspPillarCenter,   ControlID::SP_CENTER },
                        { ui->m_pspPillarRight,    ControlID::SP_RIGHT },
                        { ui->m_pslPillarLeft,     ControlID::SL_LEFT },
                        { ui->m_pslPillarCenter,   ControlID::SL_CENTER },
                        { ui->m_pslPillarRight,    ControlID::SL_RIGHT } };

    ControlID eCtrlId = ControlID::UNDEFINED_ID;

    // Determine the Control Id associated with the sender.
    const int iSize = sizeof( aControlIds ) / sizeof( aControlIds[0]);
    for ( int i = 0; i < iSize; ++i ) {
        if ( pSender == aControlIds[i].pCtrl ) {
            eCtrlId = aControlIds[i].eCtrlId;
            break;
        }
    }

    switch( eCtrlId ) {
        case ControlID::SP_LEFT: {
                ui->m_pslPillarLeft->setValue(
                            ui->m_pspPillarLeft->value() );
            }
        break;
        case ControlID::SP_CENTER: {
                ui->m_pslPillarCenter->setValue(
                            ui->m_pspPillarCenter->value() );
            }
        break;
        case ControlID::SP_RIGHT: {
                 ui->m_pslPillarRight->setValue(
                            ui->m_pspPillarRight->value() );
            }
        break;
        case ControlID::SL_LEFT: {
                const int iHeight = ui->m_pslPillarLeft->value();
                ui->m_pspPillarLeft->setValue( iHeight );
                m_PillarHeights[0] = iHeight;
                emit pillarHeightChanged();
            }
        break;
        case ControlID::SL_CENTER: {
                const int iHeight = ui->m_pslPillarCenter->value();
                ui->m_pspPillarCenter->setValue( iHeight );
                m_PillarHeights[1] = iHeight;
                emit pillarHeightChanged();
            }
        break;
        case ControlID::SL_RIGHT: {
                const int iHeight = ui->m_pslPillarRight->value();
                ui->m_pspPillarRight->setValue( iHeight );
                m_PillarHeights[2] = iHeight;
                emit pillarHeightChanged();
            }
        break;
        default: break;
    }
}
