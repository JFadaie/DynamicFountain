#pragma once

#include <QWidget>
#include "lightcontroller.h"
#include "pillarcontroller.h"
#include "watercontroller.h"
#include "snapshot.h"


class ControlManager : public QWidget
{
    Q_OBJECT
public:
    explicit ControlManager(QWidget *parent = 0);
    ~ControlManager();

    void        initLayout();

private:
    LightController*    m_pLightCtrl;
    PillarController*   m_pPillarCtrl;
    WaterController*    m_pWaterCtrl;

    SnapShot            m_CurrSnapShot;

signals:
    void        snapShotChanged(
                        const SnapShot& rSnapShot );
public slots:
    void        onControllerChanged();
    void        setSnapShot( const SnapShot& rSnapShot );

};

