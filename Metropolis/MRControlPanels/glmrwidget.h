/***************************************
 *  File: GLMRWidget
 *  Author: Josh Fadaie
 *  Date: 06/26/15
 *  Project:Metropolis
 *
 *  Description: The main widget that displays the fountain providing
 *  the user with visual feedback on the resulting structure. The
 *  widget provides the ability to rotate, zoom, and updated the
 *  fountain object.
 *
 *  Notes: None
 *
 *  References: None
 *
 ***************************************/


#pragma once


#include <QOpenGLWidget>
#include <QOpenGlShaderProgram>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QVector3D>
#include <QPoint>

#include "snapshot.h"



class GLMRWidget : public QOpenGLWidget,
                   public QOpenGLFunctions {
    Q_OBJECT

public:
                        GLMRWidget( QWidget* pParent );
                       ~GLMRWidget() {}
    void                InitCubeWidget( const QString& rqsObjFile );
protected:
    virtual void        initializeGL();
    virtual void        paintGL();
    virtual void        resizeGL(int width, int height);

    virtual void        mousePressEvent( QMouseEvent* pEvent );
    virtual void        mouseMoveEvent( QMouseEvent* pEvent );

    //virtual void        keyPressEvent( QKeyEvent* pEvent );

    // Create a zoom in and out function
    // Create grab screen method (when you shift plus drag )

private:
    QOpenGLShaderProgram*       m_pProgram;
    QMatrix4x4                  m_ModelMatrix;
    QMatrix4x4                  m_ViewMatrix;
    QMatrix4x4                  m_ProjectionMatrix;

    int                         m_iXRot;
    int                         m_iYRot;
    int                         m_iZRot;
    QPoint                      m_qptLastPos;
    QSize                       m_ViewportSize;

    SnapShot                    m_CurrSnapShot;

    void                setXRotation(int iAngle);
    void                setYRotation(int iAngle);
    void                setZRotation(int iAngle);

public slots:
    void                setSnapShot( const SnapShot& rSnapshot );


};

