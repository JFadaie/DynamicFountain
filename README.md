Consists of a 3D GUI enabling users to control water fountain features over 
time. User can modify height of water columns, pillars height and LED lights 
to create custom animations. Demoed at Saint Louis Fair, July 4th 2015 for 
STEAM outreach event.